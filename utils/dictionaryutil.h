﻿#ifndef DICTIONARYUTIL_H
#define DICTIONARYUTIL_H

#include <QMap>

class DictionaryUtil
{
private:
    QMap<QString, QMap<QString, QString>> m_SysDictionary;

public:
    DictionaryUtil();
    static DictionaryUtil *instance();
    void initDictionary();

    //插入字典
    void putDictionary(QString key, QMap<QString, QString> values);
    //读取字典
    QMap<QString, QString> getDictionary(QString key);
    //读取字典值
    QString getDictionaryValue(QString key, QString value);
};
Q_GLOBAL_STATIC(DictionaryUtil, dictInstance)

#endif // DICTIONARYUTIL_H
