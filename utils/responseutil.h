﻿#ifndef RESPONSEUTIL_H
#define RESPONSEUTIL_H

#include "QtWebApp/httpserver/httprequest.h"
#include "QtWebApp/httpserver/httpresponse.h"
#include "QtWebApp/httpserver/httprequesthandler.h"
#include "domain/resultjson.h"
#include <QImage>

using namespace stefanfrings;
class ResponseUtil
{
public:
    ResponseUtil();
    static void replyJson(HttpResponse& response, ResultJson& result);
    static void replyString(HttpResponse& response, QString result);
    static void replyImage(HttpResponse& response, QImage &image);
    static void replyHtml(HttpResponse& response, QString result);
    static void replyFile(HttpResponse& response, QString fileName);
};

#endif // RESPONSEUTIL_H
