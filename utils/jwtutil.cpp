﻿#include "jwtutil.h"
#include "global.h"

JwtUtil::JwtUtil()
{

}

JwtUtil* JwtUtil::instance(){
    return jwtInstance();  // 返回线程安全的静态对象
}

/*
 * 生成签名,5min后过期
 *
 * @param username 用户名
 * @param secret   用户的密码
 * @return 加密的token
 */
QString JwtUtil::sign(QString username, QString secret)
{
    QJsonWebToken m_jwtObj;
    m_jwtObj.setSecret(secret);
    m_jwtObj.appendClaim("username", username);
    m_jwtObj.appendClaim("expiredtime", QString::number(QDateTime::currentDateTime().addSecs(EXPIRED_TIME).toTime_t()));

    //return QString(m_jwtObj.getSignature().toHex());
    return m_jwtObj.getToken();
}


/*
 * 获得token中的信息无需secret解密也能获得
 *
 * @return token中包含的用户名
 */
QString JwtUtil::getUsername(QString token)
{
    QJsonWebToken m_jwtObj;
    m_jwtObj.setToken(token);
    return m_jwtObj.claim("username");
}
