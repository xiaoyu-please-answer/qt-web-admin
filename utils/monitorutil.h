﻿#ifndef MONITORUTIL_H
#define MONITORUTIL_H

#include <QObject>
#include <QJsonObject>
#include <QJsonArray>
#include <JQCPUMonitor>
#include <windows.h>


class MonitorMemory
{
public:
    float total;   //总内存 单位GB
    float used;    //已用内存 单位GB
    float free;    //剩余内存 单位GB
    float usage;   //使用率 单位%
    float current; //当前进程占用 单位MB

    MonitorMemory()
    {
        total = 0;
        used = 0;
        free = 0;
        usage = 0;
        current = 0;
    }

    QJsonObject toJson()
    {
        QJsonObject memInfo;
        memInfo.insert("total", total);
        memInfo.insert("used", used);
        memInfo.insert("free", free);
        memInfo.insert("usage", usage);
        memInfo.insert("current", current);
        return memInfo;
    }
};

class MonitorDisk
{
public:
    QString diskName;  //磁盘名称
    QString type;      //磁盘类型
    QString total;  //容量 单位GB
    QString free;   //空闲 单位GB
    QString used;   //已使用 单位GB
    float usage;    //使用率 单位%

    MonitorDisk()
    {
        diskName = "";
        type = "";
        total = "";
        free = "";
        used = "";
        usage = 0;
    }

    QJsonObject toJson()
    {
        QJsonObject obj;
        obj.insert("diskName", diskName);
        obj.insert("total", total);
        obj.insert("type", type);
        obj.insert("free", free);
        obj.insert("used", used);
        obj.insert("usage", usage);
        return obj;
    }
};

class MonitorCPU
{
public:
    QString cpuName;        //CPU名称
    int cpuNumber;          //CPU核心数
    int cpuLogical;         //CPU线程数
    QString architecture;   //CPU架构
    float usage;            //当前使用率 单位%
    float usage5s;          //5S使用率 单位%
    float usage30s;         //30s钟使用率 单位%
    float usage300s;        //5分钟使用率 单位%

    MonitorCPU()
    {
        cpuName = "";
        cpuNumber = 0;
        cpuLogical = 0;
        architecture = "";
        usage = 0;
        usage5s = 0;
        usage30s = 0;
        usage300s = 0;
    }

    QJsonObject toJson()
    {
        QJsonObject obj;
        obj.insert("cpuName", cpuName);
        obj.insert("cpuNumber", cpuNumber);
        obj.insert("cpuLogical", cpuLogical);
        obj.insert("architecture", architecture);
        obj.insert("usage", usage);
        obj.insert("usage5s", usage5s);
        obj.insert("usage30s", usage30s);
        obj.insert("usage300s", usage300s);
        return obj;
    }
};

class MonitorSystem
{
public:
    QString qtVersion;      //QT版本
    QDateTime startTime;    //启动时间
    QString runSeconds;         //运行时长
    QString exeDir;         //程序路径
    QString machineName;    //主机名
    QString osVersion;      //系统版本
    QString kernelType;     //内核类型
    QString kernelVersion;  //内核版本

    MonitorSystem()
    {
        qtVersion = "";
        startTime = QDateTime::currentDateTime();
        runSeconds ="";
        exeDir = "";
        machineName = "";
        osVersion = "";
        kernelType = "";
        kernelVersion = "";
    }

    QJsonObject toJson()
    {
        QJsonObject obj;
        obj.insert("qtVersion", qtVersion);
        obj.insert("startTime", startTime.toString("yyyy-MM-dd hh:mm:ss"));
        obj.insert("runSeconds", runSeconds);
        obj.insert("exeDir", exeDir);
        obj.insert("machineName", machineName);
        obj.insert("osVersion", osVersion);
        obj.insert("kernelType", kernelType);
        obj.insert("kernelVersion", kernelVersion);
        return obj;
    }
};

class MonitorMachine
{
public:
    QString gpuName;                //QT版本
    QString baseBoardManufacturer;  //主板制造商
    QString baseBoardProduct;       //主板序列号
    QString biosVendor;             //BIOS制造商
    QString biosReleaseDate;        //BISO更新时间
    QString systemManufacturer;     //产品制造商
    QString productName;            //产品编码

    MonitorMachine()
    {
        gpuName = "";
        baseBoardManufacturer = "";
        baseBoardProduct = "";
        biosVendor = "";
        biosReleaseDate = "";
        systemManufacturer = "";
        productName = "";
    }

    QJsonObject toJson()
    {
        QJsonObject obj;
        obj.insert("gpuName", gpuName);
        obj.insert("baseBoardManufacturer", baseBoardManufacturer);
        obj.insert("baseBoardProduct", baseBoardProduct);
        obj.insert("biosVendor", biosVendor);
        obj.insert("biosReleaseDate", biosReleaseDate);
        obj.insert("systemManufacturer", systemManufacturer);
        obj.insert("productName", productName);
        return obj;
    }
};


class MonitorData
{
public:
    MonitorMemory memory;
    QList<MonitorDisk> disks;
    MonitorCPU cpu;
    MonitorSystem system;
    MonitorMachine machine;

    QJsonObject toJson()
    {
        QJsonObject obj;
        obj.insert("memory", memory.toJson());
        obj.insert("cpu", cpu.toJson());
        obj.insert("system", system.toJson());
        obj.insert("machine", machine.toJson());

        QJsonArray diskArray;
        foreach(MonitorDisk disk, disks)
        {
            diskArray.append(disk.toJson());
        }
        obj.insert("disks", diskArray);
        return obj;
    }
};


class MonitorUtil
{
public:
    MonitorUtil();
    static MonitorUtil *instance();
    void init();
    void getMonitorData();

    MonitorData data;

private:
    void getUsedMemory(DWORD pid);   //WINDOWS查询程序占用内存。
    void getMemory();          //获取机器内存数据
    QString getTimeStrFromSec(qint64 seconds);
};
Q_GLOBAL_STATIC(MonitorUtil, mnrInstance)



#endif // MONITORUTIL_H
