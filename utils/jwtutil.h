﻿/*
 * JWTToken
 * 参照QJsonWebToken实现，支持功能：
 * 1、根据用户名、密码获取TOKEN
 * 2、根据用户名、密码、token判断有效性
 * 3、根据token获取用户名
 *
 * 1、登录成功后将用户的JWT生成的Token作为k、v存储到cache缓存里面(这时候k、v值一样)
 * 2、当该用户再次请求时，进行身份验证
 * 3、当该用户这次请求JWTToken值还在生命周期内，则会通过重新PUT的方式k、v都为Token值，缓存中的token值生命周期时间重新计算(这时候k、v值一样)
 * 4、当该用户这次请求jwt生成的token值已经超时，但该token对应cache中的k还是存在，则表示该用户一直在操作只是JWT的token失效了，程序会给token对应的k映射的v值重新生成JWTToken并覆盖v值，该缓存生命周期重新计算
 * 5、当该用户这次请求jwt在生成的token值已经超时，并在cache中不存在对应的k，则表示该用户账户空闲超时，返回用户信息已失效，请重新登录。
 * 6、每次当返回为true情况下，都会给Response的Header中设置Authorization，该Authorization映射的v为cache对应的v值。
 * 7、注：当前端接收到Response的Header中的Authorization值会存储起来，作为以后请求token使用
 *
 *
 */

#ifndef JWTUTIL_H
#define JWTUTIL_H

#include "QJsonWebToken/src/qjsonwebtoken.h"
#include <QObject>


class JwtUtil
{
public:
    JwtUtil();
    static JwtUtil *instance();
    static const long EXPIRED_TIME = 3600;

  /*
   * 生成签名,5min后过期
   *
   * @param username 用户名
   * @param secret   用户的密码
   * @return 加密的token
   */
  static QString sign(QString username, QString secret);

  /*
   * 获得token中的信息无需secret解密也能获得
   *
   * @return token中包含的用户名
   */
  static QString getUsername(QString token);
};
Q_GLOBAL_STATIC(JwtUtil, jwtInstance)

#endif // JWTUTIL_H
