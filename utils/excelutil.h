﻿#ifndef EXCELUTIL_H
#define EXCELUTIL_H

#include <QObject>

class ExcelUtil
{
public:
    ExcelUtil();
    static ExcelUtil *instance();

    //导出文件
    static bool exportExcelFile(QString fileName, QList<QStringList> dataList);
    static bool exportCsvFile(QString fileName, QList<QStringList> dataList);
};
Q_GLOBAL_STATIC(ExcelUtil, excelInstance)

#endif // EXCELUTIL_H
