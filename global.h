﻿#ifndef GLOBAL_H
#define GLOBAL_H

#include "QtWebApp/templateengine/templatecache.h"
#include "QtWebApp/httpserver/httpsessionstore.h"
#include "QtWebApp/httpserver/staticfilecontroller.h"
#include "QtWebApp/httpserver/httpsession.h"
#include "QtWebApp/logging/filelogger.h"

#include "domain/resultjson.h"
#include "utils/jwtutil.h"
#include "utils/cacheapi.h"
#include "utils/responseutil.h"
#include "db/sqlhelper.h"
#include <stdio.h>

#include <QImage>
#include <QPainter>
#include <QMap>
#include <QJsonObject>
#include <QJsonArray>
#include <QTemporaryFile>

using namespace stefanfrings;

/**
  Global objects that are shared by multiple source files
  of this project.
*/

/** Cache for template files */
extern TemplateCache* templateCache;

/** Storage for session cookies */
extern  HttpSessionStore* sessionStore;

/** Controller for static files */
extern  StaticFileController* staticFileController;

/** Redirects log messages to a file */
extern  FileLogger* logger;

class GlobalFunc
{
public:
    GlobalFunc();
    static GlobalFunc *instance();

    //内存绘制验证码
    static QString drawCaptcha(QImage& image);

    //QString与JSON互转
    static QJsonObject StringToJson(QString jsonString);
    static QJsonArray StringToArray(QString jsonString);
    static QString JsonToString(QJsonObject jsonObject);
    static QString ArrayToString(QJsonArray jsonArray);
    //QString与JSON互转
    static QString getRandomSalt();

    //从属性名变为字段名 userName->user_name
    static QString getFieldName(QString propName);

    //获取当前格式化时间
    static QString getNowTimeString();

    //写本地磁盘
    static QString loadDisk(QTemporaryFile* file);

   static  QString searchConfigDir();


public:
   QString configDir;

};

Q_GLOBAL_STATIC(GlobalFunc, globalInstance)

#endif // GLOBAL_H
