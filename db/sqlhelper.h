﻿#ifndef SQLHELPER_H
#define SQLHELPER_H

#include <QSqlDatabase>
#include <QVariant>
#include <QList>
#include "domain/sysuser.h"
#include "domain/sysdept.h"
#include "domain/sysdictionary.h"
#include "domain/sysdicvalue.h"
#include "domain/sysmenu.h"
#include "domain/sysrole.h"
#include "domain/sysrolemenu.h"
#include "domain/sysuserrole.h"
#include "domain/syslog.h"
#include "domain/sysusermessage.h"
#include "domain/sysmessage.h"


class SqlHelper
{
public:
    SqlHelper();
    static SqlHelper *instance();

    //初始化数据库连接
    bool initDB(QString dbName);

    //获取用户根据用户名
    bool selectUserByName(SysUser& user, QString name);
    //获取用户列表
    void getUserList(QList<SysUser>& list, QStringList condiList, QString orderField, bool isAsc = true);
    void getUserList(QList<SysUser>& list, int& count, int page, int limit, QStringList condiList, QString orderField, bool isAsc = true);

    //获取部门列表
    void getDeptList(QList<SysDept>& list, QStringList condiList, QString orderField, bool isAsc = true);

    //获取字典列表
    void getDictList(QList<SysDictionary>& list, QStringList condiList, QString orderField, bool isAsc = true);
    void getDictValueList(QList<SysDicValue>& list, QStringList condiList, QString orderField, bool isAsc = true);

    //获取菜单列表
    void getMenuList(QList<SysMenu>& list, QStringList condiList, QString orderField, bool isAsc = true);

    //获取角色菜单关联列表
    void getRoleMenuList(QList<SysRoleMenu>& list, QStringList condiList, QString orderField, bool isAsc = true);

    //获取角色列表
    void getRoleList(QList<SysRole>& list, QStringList condiList, QString orderField, bool isAsc = true);

    //根据角色获取权限菜单
    void getMenuByRoleId(QList<SysMenu>& menuList, int roleId);

    //获取用户角色关联列表
    void getUserRoleList(QList<SysUserRole>& list, QStringList condiList, QString orderField, bool isAsc = true);

    //获取日志分页表
    void getLogList(QList<SysLog>& list, int& count, int page, int limit, QStringList condiList, QString orderField, bool isAsc = true);

    //获取用户消息列表
    void getSysUserMessageList(QList<SysUserMessage>& list, QStringList condiList, QString orderField, bool isAsc = true);
    void getSysUserMessageList(QList<SysUserMessage>& list, int& count, int page, int limit, QStringList condiList, QString orderField, bool isAsc = true);
    //获取系统消息列表
    void getSysMessageList(QList<SysMessage>& list, QStringList condiList, QString orderField, bool isAsc = true);
    void getSysMessageList(QList<SysMessage>& list, int& count, int page, int limit, QStringList condiList, QString orderField, bool isAsc = true);

    //根据ID查询实体
    bool selectEntityById(TableEntity* pEntity);
    //根据ID更新实体
    bool updateEntity(TableEntity* pEntity);
    //插入实体
    bool insertEntity(TableEntity* pEntity);
    //根据ID删除实体
    bool deleteEntity(TableEntity* pEntity);
    //根据条件删除
    bool deleteTable(QString tableName, QStringList condiList);

    //获取数据表的查询结果
    void getList(QList<QMap<QString, QVariant >>& result,   //查询结果
                 QString tableName,                         //查询表名
                 QString fields,                            //查询字段
                 QStringList condiList,                     //查询条件
                 QString orderField = nullptr,              //排序字段
                 bool isAsc = true);                        //排序方式
    void getList(QList<QMap<QString, QVariant >>& result,   //查询结果
                 int& count,                                //总数量
                 QString tableName,                         //查询表名
                 QString fields,                            //查询字段
                 QStringList condiList,                     //查询条件
                 int page,                                  //查询页码
                 int limit,                                 //每页数量
                 QString orderField = nullptr,              //排序字段
                 bool isAsc = true);                        //排序方式


private:
    QSqlDatabase* db;
    void getList(TableEntity& entity, QMap<QString, QVariant> conditionMap, QString orderField, bool isAsc);
};
Q_GLOBAL_STATIC(SqlHelper, sqlInstance)

#endif // SQLHELPER_H
