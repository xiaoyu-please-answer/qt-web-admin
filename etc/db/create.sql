﻿--创建部门表
DROP TABLE IF EXISTS `sys_depart`;
CREATE TABLE `sys_depart` (
  `id` integer not null primary key autoincrement , --comment 'ID',
  `version` integer DEFAULT '0' , --comment '版本号',
  `create_name` text(50) not null , --comment '创建人',
  `create_date` datetime not null , --comment '创建时间',
  `update_name` text(50) not null , --comment '更新人',
  `update_date` datetime not null , --comment '更新时间',

  `dept_no` text(50) not null , --comment '部门编号',
  `name` text(100) not null , --comment '部门名称',
  `short_name` text(50) not null , --comment '部门简称',
  `parent_id` integer not null , --comment '上级部门',
  `type`  text(10) not null , --comment '部门类型',
  `order_no`  integer not null , --comment '排序',
  `memo`text(255) default null  --comment '备注'
 );

--创建用户表
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` integer not null primary key autoincrement , --comment '用户ID',
  `version` integer DEFAULT 0 , --comment '版本号',
  `create_name` text(50) not null , --comment '创建人',
  `create_date` datetime not null , --comment '创建时间',
  `update_name` text(50) not null , --comment '更新人',
  `update_date` datetime not null , --comment '更新时间',
  `user_name` text(50) not null , --comment '用户名',
  `real_name` text(50) not null , --comment '真实姓名',
  `password` text(50) not null , --comment '密码',
  `salt` text(10) not null , --comment '盐',
  `head_img_url` text(255) not null , --comment '头像',
  `status` text(10) not null , --comment '状态',
  `sex` text(10) not null , --comment '性别',
  `dept_id` integer not null , --comment '所属部门',
  `email` text(255) DEFAULT null , --comment '邮箱',
  `tel_no`text(50) DEFAULT null , --comment '手机号',
  `user_no`text(50) DEFAULT null , --comment '用户编号',
  `memo`text(255) DEFAULT null  --comment '备注'
 );


--创建字典表
DROP TABLE IF EXISTS `sys_dic`;
CREATE TABLE `sys_dic` (
  `id` integer not null primary key autoincrement , --comment 'ID',
  `version` integer DEFAULT 0 , --comment '版本号',
  `create_name` text(50) not null , --comment '创建人',
  `create_date` datetime not null , --comment '创建时间',
  `update_name` text(50) not null , --comment '更新人',
  `update_date` datetime not null , --comment '更新时间',
  `name` text(50) not null , --comment '名称',
  `code` text(50) not null , --comment '代码',
  `memo`text(255) DEFAULT null  --comment '备注'
 );

--创建字典值表
DROP TABLE IF EXISTS `sys_dic_value`;
CREATE TABLE `sys_dic_value` (
  `id` integer not null primary key autoincrement , --comment 'ID',
  `version` integer DEFAULT 0 , --comment '版本号',
  `create_name` text(50) not null , --comment '创建人',
  `create_date` datetime not null , --comment '创建时间',
  `update_name` text(50) not null , --comment '更新人',
  `update_date` datetime not null , --comment '更新时间',
  
  `label` text(50) not null , --comment '显示值',
  `value` text(50) not null , --comment '值',
  `parent_id` integer not null  --comment ''
 );
  
--创建角色表
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` integer not null primary key autoincrement , --comment 'ID',
  `version` integer DEFAULT 0 , --comment '版本号',
  `create_name` text(50) not null , --comment '创建人',
  `create_date` datetime not null , --comment '创建时间',
  `update_name` text(50) not null , --comment '更新人',
  `update_date` datetime not null , --comment '更新时间',  
  `name` text(50) not null , --comment '角色名称',
  `parent_id` integer not null,  --comment '父角色id'
  `role_no` text(50) not null , --comment '角色编号',
  `order_no` integer not null,  --comment '排序'
  `memo` text(255) not null  --comment '备注'
 );
 
 --创建菜单表
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` integer not null primary key autoincrement , --comment 'ID',
  `version` integer DEFAULT 0 , --comment '版本号',
  `create_name` text(50) not null , --comment '创建人',
  `create_date` datetime not null , --comment '创建时间',
  `update_name` text(50) not null , --comment '更新人',
  `update_date` datetime not null , --comment '更新时间',    
  `name` text(50) not null , --comment '菜单名称',
  `parent_id` integer not null ,  --comment '父菜单id'
  `type` text(10) not null , --comment '类型',
  `url` text(255) not null , --comment '菜单地址',
  `icon` text(255) not null , --comment '图标',
  `order_no` integer not null ,  --comment '排序'
  `memo` text(255) not null --comment '备注'
 );
    
 --创建菜单角色关联表
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` integer not null primary key autoincrement , --comment 'ID',
  `version` integer DEFAULT 0 , --comment '版本号',
  `create_name` text(50) not null , --comment '创建人',
  `create_date` datetime not null , --comment '创建时间',
  `update_name` text(50) not null , --comment '更新人',
  `update_date` datetime not null , --comment '更新时间',
  `role_id` integer not null ,  --comment '关联角色id'     
  `menu_id` integer not null   --comment '关联菜单id'
 );
    
 --创建用户角色关联表
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` integer not null primary key autoincrement , --comment 'ID',
  `version` integer DEFAULT 0 , --comment '版本号',
  `create_name` text(50) not null , --comment '创建人',
  `create_date` datetime not null , --comment '创建时间',
  `update_name` text(50) not null , --comment '更新人',
  `update_date` datetime not null , --comment '更新时间',
  `user_id` integer not null ,  --comment '关联用户id'     
  `role_id` integer not null   --comment '关联角色id'     
 );
 
 --创建日志表
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` integer not null primary key autoincrement , --comment 'ID',
  `version` integer DEFAULT 0 , --comment '版本号',
  `create_name` text(50) not null , --comment '创建人',
  `create_date` datetime not null , --comment '创建时间',
  `update_name` text(50) not null , --comment '更新人',
  `update_date` datetime not null , --comment '更新时间',
  
  `type` text(10) not null ,  --comment '日志类型'     
  `name` text(50) not null ,  --comment '名称'     
  `user_name` text(50) not null ,  --comment '用户名'     
  `class_name` text(500) not null ,  --comment '操作类名'     
  `method_name` text(255) not null ,  --comment '方法名'     
  `params` text(2500) not null ,  --comment '参数'     
  `ip` text(255) not null ,  --comment 'ip'     
  `broswer` text(255) not null ,  --comment '浏览器类型'     
  `operator_time` datetime not null ,  --comment '操作时间'     
  `memo` text(255) not null   --comment '备注'     
 );
  
 --创建系统消息表
DROP TABLE IF EXISTS `func_sys_message`;
CREATE TABLE `func_sys_message` (
  `id` integer not null primary key autoincrement , --comment 'ID',
  `version` integer DEFAULT '0' , --comment '版本号',
  `create_name` text(50) not null , --comment '创建人',
  `create_date` datetime not null , --comment '创建时间',
  `update_name` text(50) not null , --comment '更新人',
  `update_date` datetime not null , --comment '更新时间',

  `title` text(100) not null , --comment '消息标题',
  `content` text(500) not null , --comment '内容',
  `type`  text(10) not null , --comment '消息类型',
  `status`  text(10) not null , --comment '消息状态',
  `public_time` datetime  --comment '发布时间'
 );
 
 --创建系统消息表
DROP TABLE IF EXISTS `func_sys_user_message`;
CREATE TABLE `func_sys_user_message` (
  `id` integer not null primary key autoincrement , --comment 'ID',
  `version` integer DEFAULT '0' , --comment '版本号',
  `create_name` text(50) not null , --comment '创建人',
  `create_date` datetime not null , --comment '创建时间',
  `update_name` text(50) not null , --comment '更新人',
  `update_date` datetime not null , --comment '更新时间',

  `title` text(100) not null , --comment '消息标题',
  `content` text(500) not null , --comment '内容',
  `type`  text(10) not null , --comment '消息类型',  
  `read_status`  text(10) not null , --comment '阅读状态',
  `delete_status`  text(10) not null , --comment '删除状态',  
  `sys_message_id` integer not null ,  --comment '系统消息id'     
  `user_id` integer not null ,  --comment '用户id'       
  `public_time` datetime not null  --comment '发布时间'
 );

--插入部门
INSERT INTO sys_depart (version, create_name, create_date, update_name, update_date, dept_no, name, short_name,  'type', order_no, memo, parent_id) VALUES
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '001', '四方集团', '四方集团',  '0', 1, null,  0),
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '002', '北京四方', '北京四方',  '1', 1, null,  1),
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '003', '武汉四方', '武汉四方',  '1', 2, null,  1),
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '0031', '开发部', '开发部',  '2', 1, null,  3),
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '0032', '财务部', '财务部',  '2', 2, null,  3),
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '0021', '商务部', '商务部',  '2', 1, null,  2),
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '0022', '财务部', '财务部',  '2', 2, null,  2),
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '0023', '客服部', '客服部',  '2', 3, null,  2),
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '00311', '开发1组', '开发1组',  '3', 1, null, 4),
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '00312', '开发2组', '开发2组',  '3', 2, null, 4);

--插入admin账户
insert INTO sys_user (`version`, `create_name`, `create_date`, `update_name`, `update_date`,
`user_name`, `real_name`, `password`, `salt`,`head_img_url`, `status`, `sex`,`dept_id`, `email`, `tel_no`, `user_no`, `memo`)
VALUES("1", "default", "2023-08-10 16:00:00", "default", "2023-08-10 16:00:00",
"admin", "管理员", "9beb38a526558e06bdf1215c478a1375", "4mvrFegzgm",
"/", "1", "0",4, null, null, null, null);

--插入系统菜单
 INSERT INTO sys_menu (version, create_name, create_date, update_name, update_date, name, parent_id, "type", url, icon, order_no, memo) VALUES
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '项目介绍', 0, '0', '',  'layui-icon-home',          0, ''),			-- 1 项目介绍 
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '系统管理', 0, '0', '',  'layui-icon-component', 1, ''),			-- 2 系统管理
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '系统监控', 0, '0', '',  'layui-icon-console',       2, ''),			-- 3 系统监控
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '常用功能', 0, '0', '',  'layui-icon-set',              3, ''),			-- 4 常用功能
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '示例组件', 0, '0', '',  'layui-icon-layer',           4, ''),			-- 5 示例组件

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '项目主页', 1, '0', 'index',                  '',  0, ''),				-- 6 项目介绍 ->项目主页

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '用户管理', 2, '0', 'sys/user/index',    	'',  0, ''),			-- 7 系统管理 ->用户管理
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '部门管理', 2, '0', 'sys/dept/index',   	'',  1, ''),	    	-- 8 系统管理 ->部门管理
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '角色管理', 2, '0', 'sys/role/index',     	'',  2, ''),			-- 9 系统管理 ->角色管理
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '菜单管理', 2, '0', 'sys/menu/index',	'',  3, ''),			-- 10 系统管理 ->菜单管理
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '日志管理', 2, '0', 'sys/log/index',      	'',  4, ''),			-- 11 系统管理 ->日志管理
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '字典管理', 2, '0', 'sys/dic/index',       	'',  5,  ''),	    	-- 12 系统管理 ->数据字典管理
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '个人设置', 2, '0', 'sys/user/index',   	'',  6, ''),			-- 13 系统管理 ->个人设置

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', 'druid监控',   3, '0', 'monitor/druid/sql',    	'',  0, ''),		--14 系统监控 ->druid监控
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '服务器监控', 3, '0', 'monitor/server/index',	'',  1, ''),		--15 系统监控 ->服务器监控

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '邮件发送', 		4, '0', 'func/email/index',     		'',  0, ''),		--16 常用功能 ->邮件发送
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '短信服务', 		4, '0', '',     								'',  1, ''),		--17 常用功能 ->短信服务
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '定时任务', 		4, '0', '',     								'',  2, ''),		--18 常用功能 ->定时任务
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '富文本编辑器', 	4, '0', '',										'',  3, ''),		--19 常用功能 ->富文本编辑器
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '上传下载', 		4, '0', 'func/upload/upload',		'',  4, ''),		--20 常用功能 ->上传下载
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '导入导出', 		4, '0', 'func/export/index',     	'',  5, ''),		--21 常用功能 ->导入导出
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '系统消息', 		4, '0', 'func/message/sys/index','',  6, ''),		--22 常用功能 ->系统消息
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '验证码', 			4, '0', 'func/captcha/captcha',	'',  7, ''),		--23 常用功能 ->验证码

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '栅格', 			5, '0', '',											'',  0, ''),		--24 示例组件 ->栅格
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '按钮', 			5, '0', 'comp/button/index',			'',  1, ''),		--25 示例组件 ->按钮
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '表单', 			5, '0', '',											'',  2, ''),		--26 示例组件 ->表单
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '导航', 			5, '0', 'comp/nav/index',				'',  3, ''),		--27 示例组件 ->导航
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '选项卡', 		5, '0', 'comp/tabs/index',				'',  4, ''),		--28 示例组件 ->选项卡
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '进度条', 		5, '0', 'comp/progress/index',		'',  5, ''),		--29 示例组件 ->进度条
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '面板', 			5, '0', 'comp/panel/index',				'',  6, ''),		--30 示例组件 ->面板
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '徽章', 			5, '0', 'comp/badge/index',			'',  7, ''),		--31 示例组件 ->徽章
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '时间线', 		5, '0', 'comp/timeline/index',			'',  8, ''),		--32 示例组件 ->时间线
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '动画', 			5, '0', 'comp/anim/index',				'',  9, ''),		--33 示例组件 ->动画
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '辅助', 			5, '0', 'comp/auxiliar/index',			'',  10, ''),		--34 示例组件 ->辅助
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '上传', 			5, '0', '',											'',  11, ''),		--35 示例组件 ->上传
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '通用弹层', 	5, '0', '',											'',  12, ''),		--36 示例组件 ->通用弹层
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '时间日期', 	5, '0', '',											'',  13, ''),		--37 示例组件 ->时间日期
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '静态表格', 	5, '0', 'comp/table/static',				'',  14, ''),		--38 示例组件 ->静态表格
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '数据表格', 	5, '0', '',											'',  15, ''),		--39 示例组件 ->数据表格
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '分页', 			5, '0', '',											'',  16, ''),		--40 示例组件 ->分页
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '上传', 			5, '0', '',											'',  17, ''),		--41 示例组件 ->上传
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '颜色选择器',	5, '0', 'comp/colorpicker/index',	'',  18, ''),		--42 示例组件 ->颜色选择器
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '滑块组件', 	5, '0', 'comp/slider/index',				'',  19, ''),		--43 示例组件 ->滑块组件
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '评分', 			5, '0', 'comp/rate/index',				'',  20, ''),		--44 示例组件 ->评分
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '轮播', 			5, '0', 'comp/carousel/index',			'',  21, ''),		--45 示例组件 ->轮播
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '工具', 			5, '0', 'comp/util/index',				'',  22, ''),		--46 示例组件 ->工具
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '流加载', 		5, '0', 'comp/flow/index',				'',  23, ''),		--47 示例组件 ->流加载
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '代码修饰', 	5, '0', 'comp/code/index',				'',  24, ''),		--48 示例组件 ->代码修饰

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '基本资料',  13, '0', 'sys/set/info',				'',  0, ''),		--49 系统管理 ->个人设置 ->基本资料
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '修改密码',  13, '0', 'sys/set/password',				'',  1, ''),		--50 系统管理 ->个人设置 ->修改密码

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '腾讯云短信', 	17, '0', 'func/sms/qcloudsms',	'',  0, ''),		--51 常用功能 ->短信服务 ->腾讯云短信
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '云潮云短信', 	17, '0', 'func/sms/yunchaoyun',	'',  1, ''),		--52 常用功能 ->短信服务 ->云潮云短信
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '阿里云短信', 	17, '0', 'func/sms/alisms',			'',  2, ''),		--53 常用功能 ->短信服务 ->阿里云短信

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '定时任务', 	18, '0', 'func/timer/job/index',		'',  0, ''),		--54 常用功能 ->定时任务 ->定时任务
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '任务日志', 	18, '0', 'func/timer/joblog/index',	'',  1, ''),		--55 常用功能 ->定时任务 ->任务日志

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', 'layedit 编辑器', 	19, '0', 'func/layedit/layedit',		'',  0, ''),		--56 常用功能 ->富文本编辑器 ->layedit 编辑器
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', 'kz.layedit 编辑器', 19, '0', 'func/layedit/kzlayedit',	'',  1, ''),		--57 常用功能 ->富文本编辑器 ->kz.layedit 编辑器
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', 'tinymce 编辑器', 	19, '0', 'func/layedit/tinymce',	'',  2, ''),		--58 常用功能 ->富文本编辑器 ->tinymce 编辑器

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '等比例列表排列', 	24, '0', 'comp/grid/list',				'',  0, ''),		--59 示例组件 ->栅格 ->等比例列表排列
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '按移动端排列', 		24, '0', 'comp/grid/mobile',		'',  1, ''),		--60 示例组件 ->栅格 ->按移动端排列
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '移动桌面端组合', 	24, '0', 'comp/grid/mobile-pc',	'',  2, ''),		--61 示例组件 ->栅格 ->移动桌面端组合
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '全端复杂组合', 		24, '0', 'comp/grid/all',				'',  3, ''),		--62 示例组件 ->栅格 ->全端复杂组合
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '低于桌面堆叠排列',24, '0', 'comp/grid/stack',			'',  4, ''),		--63 示例组件 ->栅格 ->低于桌面堆叠排列
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '九宫格', 				24, '0', 'comp/grid/speed-dial',	'',  5, ''),		--64 示例组件 ->栅格 ->九宫格

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '表单元素', 			26, '0', 'comp/form/element',	'',  0, ''),		--65 示例组件 ->表单->表单元素
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '表单组合', 			26, '0', 'comp/form/group',		'',  1, ''),		--66 示例组件 ->表单->表单组合

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '功能演示', 	36, '0', 'comp/layer/list',					'',  0, ''),		--67 示例组件 ->通用弹层->功能演示
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '特殊示例', 	36, '0', 'comp/layer/special-demo',	'',  1, ''),		--68 示例组件 ->通用弹层->特殊示例
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '风格定制', 	36, '0', 'comp/layer/theme',				'',  2, ''),		--69 示例组件 ->通用弹层->风格定制

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '功能示例一', 37, '0', 'comp/laydate/demo1',			'',  0, ''),		--70 示例组件 ->时间日期->功能示例一
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '功能示例二', 37, '0', 'comp/laydate/demo2',			'',  1, ''),		--71 示例组件 ->时间日期->功能示例二
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '设定主题', 	37, '0', 'comp/laydate/theme',			'',  2, ''),		--72 示例组件 ->时间日期->设定主题
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '特殊示例', 	37, '0', 'comp/laydate/special-demo','',  3, ''),		--72 示例组件 ->时间日期->特殊示例

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '简单数据表格', 			39, '0', 'comp/table/simple',		'',  0, ''),		--74 示例组件 ->数据表格->简单数据表格
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '列宽自动分配', 			39, '0', 'comp/table/auto',			'',  1, ''),		--75 示例组件 ->数据表格->列宽自动分配
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '赋值已知数据', 			39, '0', 'comp/table/data',			'',  2, ''),		--76 示例组件 ->数据表格->赋值已知数据
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '转化静态表格', 			39, '0', 'comp/table/tostatic',		'',  3, ''),		--77 示例组件 ->数据表格->转化静态表格
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '开启分页', 				39, '0', 'comp/table/page',		'',  4, ''),		--78 示例组件 ->数据表格->开启分页
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '自定义分页', 			39, '0', 'comp/table/resetPage',	'',  5, ''),		--79 示例组件 ->数据表格->自定义分页
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '开启头部工具栏', 		39, '0', 'comp/table/toolbar',		'',  6, ''),		--80 示例组件 ->数据表格->开启头部工具栏
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '开启合计行', 			39, '0', 'comp/table/totalRow',	'',  7, ''),		--81 示例组件 ->数据表格->开启合计行
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '高度最大适应', 			39, '0', 'comp/table/height',		'',  8, ''),		--82 示例组件 ->数据表格->高度最大适应
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '开启复选框', 			39, '0', 'comp/table/checkbox',	'',  9, ''),		--83 示例组件 ->数据表格->开启复选框
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '开启单选框', 			39, '0', 'comp/table/radio',		'',  10, ''),		--84 示例组件 ->数据表格->开启单选框
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '开启单元格编辑', 		39, '0', 'comp/table/cellEdit',		'',  11, ''),		--85 示例组件 ->数据表格->开启单元格编辑
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '加入表单元素', 			39, '0', 'comp/table/form',			'',  12, ''),		--86 示例组件 ->数据表格->加入表单元素
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '设置单元格样式', 		39, '0', 'comp/table/style',			'',  13, ''),		--87 示例组件 ->数据表格->设置单元格样式
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '固定列', 					39, '0', 'comp/table/fixed',			'',  14, ''),		--88 示例组件 ->数据表格->固定列
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '数据操作', 				39, '0', 'comp/table/operate',	'',  15, ''),		--89 示例组件 ->数据表格->数据操作
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '解析任意数据格式', 	39, '0', 'comp/table/parseData',	'',  16, ''),		--90 示例组件 ->数据表格->解析任意数据格式
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '监听行事件', 			39, '0', 'comp/table/onrow',		'',  17, ''),		--91 示例组件 ->数据表格->监听行事件
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '数据表格的重载', 		39, '0', 'comp/table/reload',		'',  18, ''),		--92 示例组件 ->数据表格->数据表格的重载
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '设置初始排序', 			39, '0', 'comp/table/initSort',		'',  19, ''),		--93 示例组件 ->数据表格->设置初始排序
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '监听单元格事件', 		39, '0', 'comp/table/cellEvent',	'',  20, ''),		--94 示例组件 ->数据表格->监听单元格事件
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '复杂表头', 				39, '0', 'comp/table/thead',		'',  21, ''),		--95 示例组件 ->数据表格->复杂表头

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '功能演示一', 		40, '0', 'comp/laypage/demo1',		'',  0, ''),		--96 示例组件 ->分页->功能演示一
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '功能演示一', 		40, '0', 'comp/laypage/demo2',		'',  1, ''),		--97 示例组件 ->分页->功能演示一

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '功能演示一', 		41, '0', 'comp/upload/demo1',		'',  0, ''),		--98 示例组件 ->上传->功能演示一
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '功能演示一', 		41, '0', 'comp/upload/demo2',		'',  1, ''),		--99 示例组件 ->上传->功能演示一

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '新增', 7, '1', 'sys/user/add',   	'',  0, ''),	    	-- 100 系统管理 ->用户管理->新增
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '保存', 7, '1', 'sys:user:save',   		'',  1, ''),	    	-- 101 系统管理 ->用户管理->保存
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '删除', 7, '1', 'sys:user:delete',   	'',  2, ''),	    	-- 102 系统管理 ->用户管理->删除

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '新增', 8, '1', 'sys/dept/add',   	'',  0, ''),	    	-- 103 系统管理 ->部门管理->新增
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '保存', 8, '1', 'sys:dept:save',   	'',  1, ''),	    -- 104 系统管理 ->部门管理->保存
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '删除', 8, '1', 'sys:dept:delete',   	'',  2, ''),	    	-- 105 系统管理 ->部门管理->删除

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '新增', 		9, '1', 'sys/role/add',   				'',  0, ''),	    	-- 106 系统管理 ->角色管理->新增
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '保存', 		9, '1', 'sys:role:save',   				'',  1, ''),	    	-- 107 系统管理 ->角色管理->保存
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '删除', 		9, '1', 'sys:role:delete',   				'',  2, ''),	    	-- 108 系统管理 ->角色管理->删除
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '保存权限', 9, '1', 'sys:role:saverolemenu',   '',   3, ''),	    	-- 109 系统管理 ->角色管理->保存权限

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '新增', 		10, '1', 'sys/menu/add',   			'',  0, ''),	    	-- 110 系统管理 ->菜单管理->新增
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '保存', 		10, '1', 'sys:menu:save',   			'',  1, ''),	    	-- 111 系统管理 ->菜单管理->保存
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '删除', 		10, '1', 'sys:menu:delete',   		'',  2, ''),	    	-- 112 系统管理 ->菜单管理->删除

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '新增字典', 	12, '1', 'sys/dic/add',   		'',  0, ''),	-- 113 系统管理 ->字典管理->新增字典
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '新增字典值', 12, '1', 'sys/dic/addval',   	'',  1, ''),	-- 114 系统管理 ->字典管理->新增字典值
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '保存字典', 	12, '1', 'sys:dic:save',   		'',  2, ''),	-- 115 系统管理 ->字典管理->保存字典
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '保存字典值', 12, '1', 'sys:dic:saveval',   	'',  3, ''),	-- 116 系统管理 ->字典管理->保存字典值
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '删除', 			12, '1', 'sys:dic:delete',  		'',  4, ''),	-- 117 系统管理 ->字典管理->删除

(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '示例主页一', 1, '0', 'demo/main/homepage1',	'',  1, ''),	-- 118 项目介绍 ->示例主页一
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '示例主页二', 1, '0', 'demo/main/homepage2',	'',  2, ''),	-- 119 项目介绍 ->示例主页二
(1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '示例主页三', 1, '0', 'demo/main/homepage3',	'',  3, '')		-- 120 项目介绍 ->示例主页三
;

--插入角色
insert into sys_role (version, create_name, create_date, update_name, update_date, name, parent_id, role_no, order_no, memo) values
 (1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '管理员',    0, '001', 1, ''),
 (1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '技术经理', 1, '011', 1, ''),
 (1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '技术人员', 2, '111', 1, ''),
 (1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '市场人员', 1, '012', 2, ''),
 (1, 'system', '2023-08-01 10:00:00', 'system', '2023-08-01 10:00:00', '销售人员', 1, '013', 3, '') ;
 
 
 --插入字典
INSERT INTO sys_dic
(version, create_name, create_date, update_name, update_date, name, code, memo)VALUES
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00",	'普通性别', 	'common_sex', null),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00",	'部门类型', 	'dept_type', null),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00",	'表单类型', 	'form_type', null),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00",	'系统消息状态', 	'func_sys_message_status', null),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00",	'系统消息类型', 	'func_sys_message_type', null),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00",	'日志类型', 	'log_type', null),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00",	'菜单类型', 	'menu_type', null),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00",	'是否全部用户', 	'user_isall', null),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00",	'用户状态', 	'user_status', null),
("1", "system", "2023-08-02 00:00:00", "system", "2023-08-02 00:00:00",	'流程权限类型', 	'wf_per_type', null),
("1", "system", "2023-08-02 00:00:00", "system", "2023-08-02 00:00:00",	'流程状态', 	'workflow_status', null),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00",	'定时任务并发执行', 	'func_timer_job_concurrent_status', null),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00",	'定时任务执行状态', 	'func_timer_job_execute_status', null),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00",	'定时任务分组', 	'func_timer_job_group', null),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00",	'定时任务日志状态', 	'func_timer_job_log_status', null),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00",	'定时任务执行策略', 	'func_timer_job_misfire_policy', null),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00",	'定时任务状态', 	'func_timer_job_status', null);
 
 
INSERT INTO sys_dic_value
(version, create_name, create_date, update_name, update_date, label, value, parent_id) VALUES
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '男', '0', (select id from sys_dic where code = 'common_sex' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '女', '1', (select id from sys_dic where code = 'common_sex' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '集团', '0', (select id from sys_dic where code = 'dept_type' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '公司', '1', (select id from sys_dic where code = 'dept_type' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '部门', '2', (select id from sys_dic where code = 'dept_type' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '小组', '3', (select id from sys_dic where code = 'dept_type' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '系统表单', '0', (select id from sys_dic where code = 'form_type' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '自定义表单', '1', (select id from sys_dic where code = 'form_type' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '草稿', '0', (select id from sys_dic where code = 'func_sys_message_status' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '已发布', '1', (select id from sys_dic where code = 'func_sys_message_status' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '系统消息', '0', (select id from sys_dic where code = 'func_sys_message_type' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '其他', '1', (select id from sys_dic where code = 'func_sys_message_type' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '登录', '1', (select id from sys_dic where code = 'log_type' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '登出', '2', (select id from sys_dic where code = 'log_type' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '保存', '3', (select id from sys_dic where code = 'log_type' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '新增', '4', (select id from sys_dic where code = 'log_type' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '删除', '5', (select id from sys_dic where code = 'log_type' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '更新', '6', (select id from sys_dic where code = 'log_type' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '定时任务', '7', (select id from sys_dic where code = 'log_type' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '正常', '8', (select id from sys_dic where code = 'log_type' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '异常', '9', (select id from sys_dic where code = 'log_type' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '菜单', '0', (select id from sys_dic where code = 'menu_type' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '按钮', '1', (select id from sys_dic where code = 'menu_type' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '全部用户', '0', (select id from sys_dic where code = 'user_isall' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '指定用户', '1', (select id from sys_dic where code = 'user_isall' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '禁用', '0', (select id from sys_dic where code = 'user_status' limit 1)),
("1", "system", "2023-08-01 00:00:00", "system", "2023-08-01 00:00:00", '正常', '1', (select id from sys_dic where code = 'user_status' limit 1)),
("1", "system", "2023-08-02 00:00:00", "system", "2023-08-02 00:00:00", '所有人', '0', (select id from sys_dic where code = 'wf_per_type' limit 1)),
("1", "system", "2023-08-02 00:00:00", "system", "2023-08-02 00:00:00", '指定人', '1', (select id from sys_dic where code = 'wf_per_type' limit 1)),
("1", "system", "2023-08-02 00:00:00", "system", "2023-08-02 00:00:00", '正常', '0', (select id from sys_dic where code = 'workflow_status' limit 1)),
("1", "system", "2023-08-02 00:00:00", "system", "2023-08-02 00:00:00", '停用', '1', (select id from sys_dic where code = 'workflow_status' limit 1)),
("1", "system", "2023-08-02 00:00:00", "system", "2023-08-02 00:00:00", '过期', '2', (select id from sys_dic where code = 'workflow_status' limit 1)),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00", '禁止', '0', (select id from sys_dic where code = 'func_timer_job_concurrent_status' limit 1)),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00", '允许', '1', (select id from sys_dic where code = 'func_timer_job_concurrent_status' limit 1)),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00", '就绪', '0', (select id from sys_dic where code = 'func_timer_job_execute_status' limit 1)),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00", '正在执行', '1', (select id from sys_dic where code = 'func_timer_job_execute_status' limit 1)),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00", '完成', '2', (select id from sys_dic where code = 'func_timer_job_execute_status' limit 1)),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00", '暂停', '3', (select id from sys_dic where code = 'func_timer_job_execute_status' limit 1)),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00", '异常', '4', (select id from sys_dic where code = 'func_timer_job_execute_status' limit 1)),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00", '默认', 'DEFAULT', (select id from sys_dic where code = 'func_timer_job_group' limit 1)),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00", '系统', 'SYSTEM', (select id from sys_dic where code = 'func_timer_job_group' limit 1)),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00", '成功', '0', (select id from sys_dic where code = 'func_timer_job_log_status' limit 1)),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00", '失败', '1', (select id from sys_dic where code = 'func_timer_job_log_status' limit 1)),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00", '立即执行', '1', (select id from sys_dic where code = 'func_timer_job_misfire_policy' limit 1)),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00", '执行一次', '2', (select id from sys_dic where code = 'func_timer_job_misfire_policy' limit 1)),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00", '放弃执行', '3', (select id from sys_dic where code = 'func_timer_job_misfire_policy' limit 1)),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00", '启用', '0', (select id from sys_dic where code = 'func_timer_job_status' limit 1)),
("1", "system", "2023-08-03 00:00:00", "system", "2023-08-03 00:00:00", '暂停', '3', (select id from sys_dic where code = 'func_timer_job_status' limit 1));

--插入角色
INSERT INTO sys_role (version,create_name,create_date,update_name,update_date,name,parent_id,role_no,order_no,memo) VALUES
	 (1,'system','2023-08-01 10:00:00','system','2023-08-01 10:00:00','管理员',0,'001',1,''),
	 (1,'system','2023-08-01 10:00:00','system','2023-08-01 10:00:00','技术经理',1,'011',1,''),
	 (1,'system','2023-08-01 10:00:00','system','2023-08-01 10:00:00','技术人员',2,'111',1,''),
	 (1,'system','2023-08-01 10:00:00','system','2023-08-01 10:00:00','市场人员',1,'012',2,''),
	 (1,'system','2023-08-01 10:00:00','system','2023-08-01 10:00:00','销售人员',1,'013',3,'');

--给管理员账户设置管理员角色
INSERT INTO sys_user_role (version,create_name,create_date,update_name,update_date,user_id,role_id) VALUES
	 (1,'admin','2023-08-30 08:57:48','admin','2023-08-30 08:57:48',1,1);
	 
--插入管理员角色菜单
INSERT INTO sys_role_menu (version,create_name,create_date,update_name,update_date,role_id,menu_id) VALUES
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,1),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,6),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,2),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,7),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,100),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,101),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,102),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,8),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,103),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,104);
INSERT INTO sys_role_menu (version,create_name,create_date,update_name,update_date,role_id,menu_id) VALUES
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,105),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,9),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,106),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,107),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,108),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,109),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,10),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,110),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,111),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,112);
INSERT INTO sys_role_menu (version,create_name,create_date,update_name,update_date,role_id,menu_id) VALUES
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,11),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,12),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,113),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,114),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,115),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,116),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,117),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,13),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,49),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,50);
INSERT INTO sys_role_menu (version,create_name,create_date,update_name,update_date,role_id,menu_id) VALUES
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,3),
	-- (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,14),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,15),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,4),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,16),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,17),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,51),
	-- (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,52),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,53),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,18);
INSERT INTO sys_role_menu (version,create_name,create_date,update_name,update_date,role_id,menu_id) VALUES
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,54),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,55),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,19),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,56),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,57),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,58),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,20),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,21),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,22),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,23);
INSERT INTO sys_role_menu (version,create_name,create_date,update_name,update_date,role_id,menu_id) VALUES
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,5),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,24),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,59),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,60),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,61),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,62),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,63),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,64),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,25),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,26);
INSERT INTO sys_role_menu (version,create_name,create_date,update_name,update_date,role_id,menu_id) VALUES
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,65),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,66),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,27),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,28),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,29),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,30),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,31),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,32),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,33),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,34);
INSERT INTO sys_role_menu (version,create_name,create_date,update_name,update_date,role_id,menu_id) VALUES
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,35),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,36),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,67),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,68),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,69),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,37),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,70),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,71),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,72),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,73);
INSERT INTO sys_role_menu (version,create_name,create_date,update_name,update_date,role_id,menu_id) VALUES
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,38),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,39),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,74),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,75),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,76),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,77),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,78),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,79),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,80),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,81);
INSERT INTO sys_role_menu (version,create_name,create_date,update_name,update_date,role_id,menu_id) VALUES
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,82),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,83),
	 (1,'admin','2023-08-30 08:58:07','admin','2023-08-30 08:58:07',1,84),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,85),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,86),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,87),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,88),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,89),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,90),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,91);
INSERT INTO sys_role_menu (version,create_name,create_date,update_name,update_date,role_id,menu_id) VALUES
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,92),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,93),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,94),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,95),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,40),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,96),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,97),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,41),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,98),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,99);
INSERT INTO sys_role_menu (version,create_name,create_date,update_name,update_date,role_id,menu_id) VALUES
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,42),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,43),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,44),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,45),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,46),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,47),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,48),
	 (1,'admin','2023-08-30 08:58:08','admin','2023-08-30 08:58:08',1,0);