﻿#include "funcmonitorcontroller.h"
#include "utils/monitorutil.h"

FuncMonitorController::FuncMonitorController()
{
    m_pHelper = SqlHelper::instance();
    m_mapFunc.insert("/monitor/server/server",  &FuncMonitorController::server);      //页面获取服务器信息
    m_mapFunc.insert("/monitor/server/index",   &FuncMonitorController::server);      //页面获取服务器信息

}

void FuncMonitorController::service(HttpRequest& request, HttpResponse& response)
{
    ResultJson ret(0, true, QStringLiteral("操作成功"));
    QString access_token = request.getParameter("access_token");
    QString username = JwtUtil::getUsername(access_token);
    if(username == NULL || username.isEmpty())
    {
        ret.failedMsg(QStringLiteral("操作失败"));
        ResponseUtil::replyJson(response, ret);
        return;
    }
    SysUser visitUser;
    if(!m_pHelper->selectUserByName(visitUser, username))
    {
        ret.failedMsg(QStringLiteral("用户不存在，操作失败"));
        ResponseUtil::replyJson(response, ret);
        return;
    }

    QByteArray path = request.getPath().toLower();
    if(m_mapFunc.contains(path))
    {
        pServFunc func = m_mapFunc.value(path);
        (this->*func)(request, response, visitUser);
    }
}

//页面获取服务器信息
//http://localhost:9004/monitor/server/server?access_token=?
//{"code":0,"data":{"cpu":{"architecture":"x86_64","cpuLogical":12,"cpuName":"AMD RYZEN 5 PRO 4650U WITH RADEON GRAPHICS  ","cpuNumber":6,"usage":31.522634506225586,"usage300s":9.6234264373779297,"usage30s":10.385137557983398,"usage5s":16.119840621948242},"disks":[{"diskName":"C:/","free":"17 GB","total":"138 GB","type":"NTFS","usage":87.739997863769531,"used":"121 GB"},{"diskName":"D:/","free":"24 GB","total":"372 GB","type":"NTFS","usage":93.5,"used":"348 GB"}],"machine":{"baseBoardManufacturer":"LENOVO","baseBoardProduct":"20UD0003CD","biosReleaseDate":"06/11/2020","biosVendor":"LENOVO","gpuName":"AMD RADEON GRAPHICS PROCESSOR (0X1636)  ","productName":"20UD0003CD","systemManufacturer":"LENOVO"},"memory":{"current":212.87109375,"free":4.4073143005371094,"total":15.22625732421875,"usage":71.054512023925781,"used":10.818943023681641},"system":{"exeDir":"D:/project/cpp/build-QtWebAdmin-Desktop_Qt_5_6_1_MSVC2013_64bit-Debug/debug","kernelType":"winnt","kernelVersion":"10.0.19045","machineName":"WH600191","osVersion":"windows","qtVersion":"5.6.1","runSeconds":"00:05:04","startTime":"2023-09-06 13:03:37"}},"msg":"操作成功","success":true}
void FuncMonitorController::server(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{
    ResultJson ret(0, true, QStringLiteral("操作成功"));

    MonitorUtil* util = MonitorUtil::instance();
    util->getMonitorData();
    QJsonObject data = util->data.toJson();
    ret.setData(data);
    ResponseUtil::replyJson(response, ret);
}
