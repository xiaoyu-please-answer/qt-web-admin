﻿
#include "funcvercodecontroller.h"
#include <QUuid>

FuncVercodeController::FuncVercodeController()
{

}

//前后端分离场景下，存在跨域和session不一致问题
void FuncVercodeController::service(HttpRequest& request, HttpResponse& response)
{
    QImage image(130, 40, QImage::Format_RGB32);
    QString code = GlobalFunc::drawCaptcha(image);
//    HttpSession session = sessionStore->getSession(request, response, true);
//    session.set("vercode", code);

    //跨域情况下，以网页请求时附带UUID，作为验证码标志
    QString codeid = request.getParameter("a");
    CacheApi::instance()->insert(codeid + "_vercode", code, 60);

    ResponseUtil::replyImage(response, image);
}
