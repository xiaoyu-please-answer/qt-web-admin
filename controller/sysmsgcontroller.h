﻿#ifndef SYSMSGCONTROLLER_H
#define SYSMSGCONTROLLER_H


#include "global.h"

using namespace stefanfrings;

class SysMsgController: public HttpRequestHandler {
    Q_OBJECT
    Q_DISABLE_COPY(SysMsgController)

public:
    SysMsgController();
    void  service(HttpRequest& request, HttpResponse& response);

    void   status(HttpRequest& request, HttpResponse& response, SysUser& visitUser);//获取系统消息状态
    void     type(HttpRequest& request, HttpResponse& response, SysUser& visitUser);//获取系统消息类型
    void datagrid(HttpRequest& request, HttpResponse& response, SysUser& visitUser);//获取系统消息列表
    void   detail(HttpRequest& request, HttpResponse& response, SysUser& visitUser);//获取系统消息明细
    void     save(HttpRequest& request, HttpResponse& response, SysUser& visitUser);//保存系统消息
    void      del(HttpRequest& request, HttpResponse& response, SysUser& visitUser);//删除系统消息
    void  publish(HttpRequest& request, HttpResponse& response, SysUser& visitUser);//发布系统消息


private:
    SqlHelper* m_pHelper;
    typedef void (SysMsgController::*pServFunc)(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    QMap<QString, pServFunc> m_mapFunc;
};

#endif // SYSMSGCONTROLLER_H

