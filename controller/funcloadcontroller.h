﻿#ifndef FUNCLOADCONTROLLER_H
#define FUNCLOADCONTROLLER_H

#include "global.h"
#include <QMap>
using namespace stefanfrings;

class FuncLoadController: public HttpRequestHandler
{
    Q_OBJECT
    Q_DISABLE_COPY(FuncLoadController)
public:
    FuncLoadController();
    void         service(HttpRequest& request, HttpResponse& response);
    void     localupload(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    void   localdownload(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
private:
    SqlHelper* m_pHelper;
    typedef void (FuncLoadController::*pServFunc)(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    QMap<QString, pServFunc> m_mapFunc;
};

#endif // FUNCLOADCONTROLLER_H
