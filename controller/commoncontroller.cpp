﻿#include "commoncontroller.h"
#include "utils/dictionaryutil.h"

CommonController::CommonController()
{
    m_pHelper = SqlHelper::instance();
    m_mapFunc.insert("/common/sex/sextype",  &CommonController::sexType);      //获取性别类型
    m_mapFunc.insert("/common/selectdept",   &CommonController::selectDept);   //公共弹出页面,  部门的选择
    m_mapFunc.insert("/common/selectmenu",   &CommonController::selectMenu);   //公共弹出页面,  菜单的选择
    m_mapFunc.insert("/common/selectrole",   &CommonController::selectRole);   //公共弹出页面,  部门的选择
    m_mapFunc.insert("/common/selectroles",  &CommonController::selectRoles);  //公共弹出页面,  角色的选择
    m_mapFunc.insert("/common/getmenutree",  &CommonController::getMenuTree);  //公共弹出页面,  根据角色的ID，获取权限配置

}

void CommonController::service(HttpRequest& request, HttpResponse& response)
{
    ResultJson ret(0, true, QStringLiteral("操作成功"));
    QString access_token = request.getParameter("access_token");
    QString username = JwtUtil::getUsername(access_token);
    if(username == NULL || username.isEmpty())
    {
        ret.failedMsg(QStringLiteral("操作失败"));
        ResponseUtil::replyJson(response, ret);
        return;
    }
    SysUser visitUser;
    if(!m_pHelper->selectUserByName(visitUser, username))
    {
        ret.failedMsg(QStringLiteral("用户不存在，操作失败"));
        ResponseUtil::replyJson(response, ret);
        return;
    }

    QByteArray path = request.getPath().toLower();
    if(m_mapFunc.contains(path))
    {
        pServFunc func = m_mapFunc.value(path);
        (this->*func)(request, response, visitUser);
    }
}

//http://localhost:9004/common/sex/sexType?access_token=?
//{"code":0,"data":[{"label":"男","value":"0"},{"label":"女","value":"1"}],"msg":""}
void CommonController::sexType(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{    
    ResultJson ret(0, true, QStringLiteral("操作成功"));
    QMap<QString, QString> map = DictionaryUtil::instance()->getDictionary("common_sex");

    QMap<QString, QJsonObject> orderMap;
    foreach (QString key, map.keys()) {
         QJsonObject obj;
         obj.insert("label", key);
         obj.insert("value", map[key]);
         orderMap.insert(map[key], obj);
    }
    QJsonArray dataArray;
    foreach (QString key, orderMap.keys())
        dataArray.append(orderMap[key]);
    ret.setData(dataArray);
    ResponseUtil::replyJson(response, ret);
}

//http://localhost:9004/common/selectroles?access_token=?
//{"code":0,"data":[{"checkArr":[{"isChecked":"0","type":"0"}],"id":"?","parentId":"?","title":"销售人员"},{"checkArr":[{"$ref":"$.data[0].checkArr[0]"}],"id":"?","parentId":"?","title":"技术经理"},{"checkArr":[{"$ref":"$.data[0].checkArr[0]"}],"id":"?","parentId":"?","title":"技术人员"},{"checkArr":[{"$ref":"$.data[0].checkArr[0]"}],"id":"?","parentId":"0","title":"管理员"},{"checkArr":[{"$ref":"$.data[0].checkArr[0]"}],"id":"?","parentId":"?","title":"市场人员"}],"msg":""}
void CommonController::selectRoles(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{
    QList<SysRole> roleList;
    QStringList condi;
    m_pHelper->getRoleList(roleList, condi, "order_no");

    ResultJson ret(0, true, QStringLiteral(""));
    QJsonArray array;
    foreach (SysRole role, roleList) {

        QJsonArray checkArr;
        QJsonObject checkObj;
        checkObj.insert("type",  "0");
        checkObj.insert("isChecked", "0");
        checkArr.append(checkObj);

        QJsonObject obj;
        obj.insert("id",        role.getId());
        obj.insert("parentId",  role.getParentId());
        obj.insert("title",     role.getName());
        obj.insert("checkArr",  checkArr);

        array.append(obj);
    }

    ret.setData(array);
    ResponseUtil::replyJson(response, ret);
}

//http://localhost:9004/common/selectdept?access_token=？
//{"code":0,"data":[{"id":"？","parentId":"？","title":"开发1组"},{"id":"?","parentId":"/","title":"gg"},{"id":"?","parentId":"?","title":"江苏众禾"},{"id":"?","parentId":"?","title":"商务部"},{"id":"?","parentId":"?","title":"开发部"},{"id":"?","parentId":"0","title":"众禾集团"},{"id":"?","parentId":"?","title":"上海华谷"},{"id":"?","parentId":"?","title":"财务部"},{"id":"?","parentId":"?","title":"市场部"},{"id":"？","parentId":"？","title":"客服部"}],"msg":""}
void CommonController::selectDept(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{
    QStringList condi;
    QList<SysDept> deptList;
    m_pHelper->getDeptList(deptList, condi, "order_no");

    QJsonArray dataArray;
    foreach (SysDept dept, deptList) {
         QJsonObject obj;
         obj.insert("id",           dept.getId());
         obj.insert("title",        dept.getName());
         obj.insert("parentId",     dept.getParentId());
         dataArray.append(obj);
    }

    ResultJson ret(0, true, QStringLiteral(""));
    ret.setData(dataArray);
    ResponseUtil::replyJson(response, ret);
}

//http://localhost:9004/common/selectmenu?access_token=？
//{"code":0,"data":[{"id":"36df3e985a7c01432cc579728425bf09","parentId":"0","title":"项目介绍"},{"id":"87c8451f951888e483fe7ec7668507fe","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"栅格"},{"id":"0d079901d30c6bdbcae1349de1aabd30","parentId":"e04db2a1c2d8d911b6961f0c29ffe02a","title":"功能示例一"},{"id":"391f39b83b80cc3bd68e115d935f8109","parentId":"f9679bc12890ed472d0fec02dd3fad64","title":"功能演示一"},{"id":"3b362bf507f42d306368728df86877e0","parentId":"76cf30bf5db55a9204e38dec11079e2a","title":"功能演示一"},{"id":"426c5207e1f95db3ca968ec508aea490","parentId":"a03f6123a7a1be65e514edd28de0ae8d","title":"基本资料"},{"id":"4f8fa64d0ec7fce7c2cedc5ac6d09489","parentId":"91be39ac09cdc71c7435ffb94f611795","title":"邮件发送"},{"id":"59f249d45a9478beae170b4a4ce03b61","parentId":"254d304678038641f87aff42be68ef74","title":"简单数据表格"},{"id":"623262eac50915905031d8d67d8702ea","parentId":"0","title":"系统管理"},{"id":"700bb6c4a8d5cd6b9d7016e4faae8628","parentId":"36df3e985a7c01432cc579728425bf09","title":"项目主页"},{"id":"755956ba2ea812d8749a062f34b7da2d","parentId":"623262eac50915905031d8d67d8702ea","title":"用户管理"},{"id":"75e8d5210cf7c50af4db8f560c020974","parentId":"7de00ce887ea0c9c7144636112801a0c","title":"新增"},{"id":"84cc6a4dbda00fc1d3a24d55c9cd14e6","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"按钮"},{"id":"a7194554d65798806bac6732d36f67a4","parentId":"524ab0ea6d1bd06ea8f7430acf258b41","title":"功能演示"},{"id":"aa04121cc3d2e79286103acc24c35770","parentId":"755956ba2ea812d8749a062f34b7da2d","title":"新增"},{"id":"ae1dd4b597a544660759211fa1481b1b","parentId":"6ee28a2031c8d1e701613082ea6ebc5e","title":"新增"},{"id":"ae6e226f18c93b6fc93c1b6817242741","parentId":"a9adea345ac1afeb3292e576bbfec804","title":"表单元素"},{"id":"be09904bbab6e8ad917bf6bf2d3c6ce7","parentId":"213830032d2b72edf575f30d71361a51","title":"定时任务"},{"id":"c98b436c0bdf3e1db2aacbbaa6fcaca4","parentId":"3c02556f5054c9fdf6faf6574004e792","title":"新增字典"},{"id":"d4f0ee03c841e22d7cb1cbd271c63100","parentId":"dd22d35e3d8fc63fa51ff4e01668fab6","title":"layedit 编辑器"},{"id":"da49ca0fe2ea8be4bb557c251c6180c3","parentId":"7476783545496331c211296efaa561a2","title":"druid 监控"},{"id":"fe70cf2fa1985a461186efcaf4633283","parentId":"87c8451f951888e483fe7ec7668507fe","title":"等比例列表排列"},{"id":"ff358bb4ca406915664f8cb8b848e9da","parentId":"ecacd9efd6c237b80e46df8b1507a904","title":"新增"},{"id":"19587742caad08e1c5eaa9483b02f49b","parentId":"755956ba2ea812d8749a062f34b7da2d","title":"保存"},{"id":"1fc380fb7a2b3992e5f0dce39a683ce3","parentId":"28d1d2a835dc364315cc7f9832d664a0","title":"腾讯云短信"},{"id":"4b2e9da372c5c7b5d4d39c0f9a4e402d","parentId":"7476783545496331c211296efaa561a2","title":"服务器监控"},{"id":"6ee28a2031c8d1e701613082ea6ebc5e","parentId":"623262eac50915905031d8d67d8702ea","title":"部门管理"},{"id":"73c52763764317a58bb2b7db884e8788","parentId":"f9679bc12890ed472d0fec02dd3fad64","title":"功能演示二"},{"id":"7476783545496331c211296efaa561a2","parentId":"0","title":"系统监控"},{"id":"7e106236ac4833f27ffa0250d3492071","parentId":"36df3e985a7c01432cc579728425bf09","title":"示例主页一"},{"id":"83b3db7973133104ba24135b453939ce","parentId":"3c02556f5054c9fdf6faf6574004e792","title":"新增字典值"},{"id":"9e278b9a4ee34914d1dd1969e800eb67","parentId":"76cf30bf5db55a9204e38dec11079e2a","title":"功能演示二"},{"id":"ac698e2709fdcb90ee8b9ed88e5905d1","parentId":"e04db2a1c2d8d911b6961f0c29ffe02a","title":"功能示例二"},{"id":"adc1f1a492ac837bfec6292265c3b30d","parentId":"dd22d35e3d8fc63fa51ff4e01668fab6","title":"kz.layedit 编辑器"},{"id":"af52b09768353133a4d3aab4f167d4c4","parentId":"ecacd9efd6c237b80e46df8b1507a904","title":"保存"},{"id":"b44ad72e6203953c8f6a74521d05c8aa","parentId":"a03f6123a7a1be65e514edd28de0ae8d","title":"修改密码"},{"id":"b7eadf3fdfe67c7ff3223c838213a584","parentId":"524ab0ea6d1bd06ea8f7430acf258b41","title":"特殊示例"},{"id":"be00c02c463f91af6433918bee8219c4","parentId":"a9adea345ac1afeb3292e576bbfec804","title":"表单组合"},{"id":"c9f52990bc82165f63e45df34077896c","parentId":"7de00ce887ea0c9c7144636112801a0c","title":"保存角色"},{"id":"d080eec50903b82466fe1aa19f78b01d","parentId":"6ee28a2031c8d1e701613082ea6ebc5e","title":"保存"},{"id":"ecf0f6a8f0a34d0587695c5351c073d3","parentId":"213830032d2b72edf575f30d71361a51","title":"任务日志"},{"id":"ee2ca956201793d7f035d2ae6e944c93","parentId":"254d304678038641f87aff42be68ef74","title":"列宽自动分配"},{"id":"f4bc6809cec463e0206deb80319dfb97","parentId":"87c8451f951888e483fe7ec7668507fe","title":"按移动端排列"},{"id":"0537fa19001c1758fe1d7e8ae93cb61a","parentId":"28d1d2a835dc364315cc7f9832d664a0","title":"阿里云短信"},{"id":"06428eaef123e23852946540710eafe7","parentId":"524ab0ea6d1bd06ea8f7430acf258b41","title":"风格定制"},{"id":"0f5a97c074f42292c477c1ecb8e58769","parentId":"3c02556f5054c9fdf6faf6574004e792","title":"保存字典"},{"id":"1b52228fe7c8b9cf259f9f7c9bfcd844","parentId":"254d304678038641f87aff42be68ef74","title":"赋值已知数据"},{"id":"207ae629bfbf53290f3c64f1933d5ae6","parentId":"36df3e985a7c01432cc579728425bf09","title":"示例主页二"},{"id":"28d1d2a835dc364315cc7f9832d664a0","parentId":"91be39ac09cdc71c7435ffb94f611795","title":"短信服务"},{"id":"3d0d3eed4985181a82a6387a30974a9c","parentId":"28d1d2a835dc364315cc7f9832d664a0","title":"云潮云短信"},{"id":"427d532a84547250e510a3dcc388c92f","parentId":"6ee28a2031c8d1e701613082ea6ebc5e","title":"删除"},{"id":"4388d13e00a1a94300c5fa7ff32d9d90","parentId":"dd22d35e3d8fc63fa51ff4e01668fab6","title":"tinymce 编辑器"},{"id":"6aa4271aff8644eeb1561f57f8320446","parentId":"755956ba2ea812d8749a062f34b7da2d","title":"删除"},{"id":"7bf0c837223ed4b2693eadc933d5de56","parentId":"ecacd9efd6c237b80e46df8b1507a904","title":"删除"},{"id":"7de00ce887ea0c9c7144636112801a0c","parentId":"623262eac50915905031d8d67d8702ea","title":"角色管理"},{"id":"7e3a22591b016b3ce6ccc881e7b9d294","parentId":"7de00ce887ea0c9c7144636112801a0c","title":"保存权限"},{"id":"8eec007fbd23c0a318ba3e74f9c3d2a9","parentId":"87c8451f951888e483fe7ec7668507fe","title":"移动桌面端组合"},{"id":"91be39ac09cdc71c7435ffb94f611795","parentId":"0","title":"常用功能"},{"id":"a9adea345ac1afeb3292e576bbfec804","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"表单"},{"id":"ab83dd17afdebc0ffe0d20318e23ea07","parentId":"e04db2a1c2d8d911b6961f0c29ffe02a","title":"设定主题"},{"id":"213830032d2b72edf575f30d71361a51","parentId":"91be39ac09cdc71c7435ffb94f611795","title":"定时任务"},{"id":"33ad4150fead0f227d7c6adb3d4fde2f","parentId":"36df3e985a7c01432cc579728425bf09","title":"示例主页三"},{"id":"40cad6aa986f987303647287352f7788","parentId":"87c8451f951888e483fe7ec7668507fe","title":"全端复杂组合"},{"id":"4618aa278da605c57895074aee4e095d","parentId":"254d304678038641f87aff42be68ef74","title":"转化静态表格"},{"id":"612265d33877f681c26b2180f90a3e62","parentId":"e04db2a1c2d8d911b6961f0c29ffe02a","title":"特殊示例"},{"id":"a5efa8ea44340e69710c398838c78989","parentId":"3c02556f5054c9fdf6faf6574004e792","title":"保存字典值"},{"id":"bf5b7351e458a15a1ad7cf4421eb9493","parentId":"7de00ce887ea0c9c7144636112801a0c","title":"删除"},{"id":"ecacd9efd6c237b80e46df8b1507a904","parentId":"623262eac50915905031d8d67d8702ea","title":"菜单管理"},{"id":"ff544ef46b64157f10b8c20a3ae33d0d","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"导航"},{"id":"2774f4f1c30a34a75dfcfe5f8945d370","parentId":"0","title":"示例组件"},{"id":"2f9b5ee346d667a5c146b3a25009bc8c","parentId":"623262eac50915905031d8d67d8702ea","title":"日志管理"},{"id":"3e5df1746ba7a3be358e41cedff4c825","parentId":"3c02556f5054c9fdf6faf6574004e792","title":"删除"},{"id":"54c7509f7a0fca385c935e4919dc10e7","parentId":"254d304678038641f87aff42be68ef74","title":"开启分页"},{"id":"77e7ae5ca528b246f72e4983374da469","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"选项卡"},{"id":"dd22d35e3d8fc63fa51ff4e01668fab6","parentId":"91be39ac09cdc71c7435ffb94f611795","title":"富文本编辑器"},{"id":"e22d3ebde93e719f9cbe9b9004c2eae8","parentId":"87c8451f951888e483fe7ec7668507fe","title":"低于桌面堆叠排列"},{"id":"1498b9a569c6e2e6562e79a834cc93f4","parentId":"91be39ac09cdc71c7435ffb94f611795","title":"上传下载"},{"id":"326147b3715dd8698f4d84058404e83d","parentId":"254d304678038641f87aff42be68ef74","title":"自定义分页"},{"id":"3c02556f5054c9fdf6faf6574004e792","parentId":"623262eac50915905031d8d67d8702ea","title":"数据字典管理"},{"id":"3ea0bdfc67e946ddc62dd82d9858b540","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"进度条"},{"id":"f8c3125fc92513c4ef9153aca7d6e272","parentId":"87c8451f951888e483fe7ec7668507fe","title":"九宫格"},{"id":"10fd29fb35ea748ae75dcd84895997c0","parentId":"91be39ac09cdc71c7435ffb94f611795","title":"导入导出"},{"id":"4553ac5aa6eb328b8f5e68506c528715","parentId":"254d304678038641f87aff42be68ef74","title":"开启头部工具栏"},{"id":"a03f6123a7a1be65e514edd28de0ae8d","parentId":"623262eac50915905031d8d67d8702ea","title":"个人设置"},{"id":"bad0d5cfbdf372cf8025d32053fa4be3","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"面板"},{"id":"67090df9bf7c076aee440df201436458","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"徽章"},{"id":"8601b3790fb4c239f5ea5a61b56cd208","parentId":"91be39ac09cdc71c7435ffb94f611795","title":"系统消息"},{"id":"b2d074869324db44caae589ceabacdcb","parentId":"254d304678038641f87aff42be68ef74","title":"开启合计行"},{"id":"3c83e94d8c9499d3962e405a2dddeebb","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"时间线"},{"id":"60fda72e475b1536cb166b0af7741697","parentId":"91be39ac09cdc71c7435ffb94f611795","title":"验证码"},{"id":"ee31b443c0eb5d142e29012f559d0ef6","parentId":"254d304678038641f87aff42be68ef74","title":"高度最大适应"},{"id":"9cee434d97d9f75d2f35b2c87bef99bf","parentId":"254d304678038641f87aff42be68ef74","title":"开启复选框"},{"id":"a68e3126a3a35f5a50141ca027c35ce8","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"动画"},{"id":"ee7e7bb6e0da465b0b686473acc9259f","parentId":"254d304678038641f87aff42be68ef74","title":"开启单选框"},{"id":"fddbacbd7477af853faac22ca34d3a55","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"辅助"},{"id":"524ab0ea6d1bd06ea8f7430acf258b41","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"通用弹层"},{"id":"7c6037cdf7687a75d823b18b76c2d03f","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"上传"},{"id":"a6645e6e95d7babef01e71effdd91d6d","parentId":"254d304678038641f87aff42be68ef74","title":"开启单元格编辑"},{"id":"de471ff81fd3b7b0708ea3fd1a493f78","parentId":"254d304678038641f87aff42be68ef74","title":"加入表单元素"},{"id":"e04db2a1c2d8d911b6961f0c29ffe02a","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"时间日期"},{"id":"50677a25a6a1cc3e3d71834ef84c2c39","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"静态表格"},{"id":"5a717dd0bdd94123900508d172088d48","parentId":"254d304678038641f87aff42be68ef74","title":"设置单元格样式"},{"id":"24ca613859f753247f86b55a13e4f7d1","parentId":"254d304678038641f87aff42be68ef74","title":"固定列"},{"id":"254d304678038641f87aff42be68ef74","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"数据表格"},{"id":"324c09d31cc6910fa5ae805a737d32a8","parentId":"254d304678038641f87aff42be68ef74","title":"数据操作"},{"id":"76cf30bf5db55a9204e38dec11079e2a","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"分页"},{"id":"6c6da7e850d5a779a1ca3f3763cbce0d","parentId":"254d304678038641f87aff42be68ef74","title":"解析任意数据格式"},{"id":"f9679bc12890ed472d0fec02dd3fad64","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"上传"},{"id":"38aba0fbe14e8c0f596ff40fb29532aa","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"颜色选择器"},{"id":"7a5be98a72343beb5bd4f73e2793acb3","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"滑块组件"},{"id":"8afeeaa1d6f5c139f8b8cecf6c451ef2","parentId":"254d304678038641f87aff42be68ef74","title":"监听行事件"},{"id":"7514b093df0e52d76814ce5a6fb5c868","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"评分"},{"id":"df3fb69cec5024ffdc1967ed729541b6","parentId":"254d304678038641f87aff42be68ef74","title":"数据表格的重载"},{"id":"afd48d274868f87f05fa4aad2d767bf9","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"轮播"},{"id":"f5872bede681da8b0a7d5a2c1a2b3146","parentId":"254d304678038641f87aff42be68ef74","title":"设置初始排序"},{"id":"22691e0c807693f3ad52dcb043a2e2f7","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"流加载"},{"id":"66f6ab8a5f40facb94ab9ff93cd14aaf","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"工具"},{"id":"9de651e122d377f59f08febfaa3a5418","parentId":"254d304678038641f87aff42be68ef74","title":"监听单元格事件"},{"id":"6abd38f3c7631bccffb49ade84dba77b","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"代码修饰"},{"id":"c8ac3f04aea6be178a23cb5f93ec71aa","parentId":"254d304678038641f87aff42be68ef74","title":"复杂表头"}],"msg":""}
void CommonController::selectMenu(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{
    QStringList condi;
    QList<SysMenu> menuList;
    m_pHelper->getMenuList(menuList, condi, "order_no");

    QJsonArray dataArray;
    foreach (SysMenu menu, menuList) {
         QJsonObject obj;
         obj.insert("id",           menu.getId());
         obj.insert("title",        menu.getName());
         obj.insert("parentId",     menu.getParentId());
         dataArray.append(obj);
    }

    ResultJson ret(0, true, QStringLiteral(""));
    ret.setData(dataArray);
    ResponseUtil::replyJson(response, ret);
}

//http://localhost:9004/common/selectrole?access_token=？
//{"code":0,"data":[{"id":"?","parentId":"?","title":"销售人员"},{"id":"3c67201c91bde61d709e4a2460f4e825","parentId":"ea291b3717f834d2add7b14e2a2464ca","title":"技术经理"},{"id":"4b860179c135ea23961cc1fb88926f97","parentId":"3c67201c91bde61d709e4a2460f4e825","title":"技术人员"},{"id":"ea291b3717f834d2add7b14e2a2464ca","parentId":"0","title":"管理员"},{"id":"a088b47539612eebeace406a8f458910","parentId":"ea291b3717f834d2add7b14e2a2464ca","title":"市场人员"}],"msg":""}
void CommonController::selectRole(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{    
    QList<SysRole> roleList;
    QStringList condi;
    m_pHelper->getRoleList(roleList, condi, "order_no");

    ResultJson ret(0, true, QStringLiteral(""));
    QJsonArray array;
    foreach(SysRole role, roleList)
    {
        QJsonObject obj;
        obj.insert("id", role.getId());
        obj.insert("title", role.getName());
        if(role.getParentId() <=0 )
            obj.insert("parentId", 0);
        else
            obj.insert("parentId", role.getParentId());

        array.append(obj);
    }

    ret.setData(array);
    ResponseUtil::replyJson(response, ret);
}

//http://localhost:9004/common/getmenutree?access_token=？
void CommonController::getMenuTree(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{

}
