﻿#ifndef LOGINCONTROLLER_H
#define LOGINCONTROLLER_H

#include "global.h"
#include <QMap>

using namespace stefanfrings;

class LoginController: public HttpRequestHandler {
    Q_OBJECT
    Q_DISABLE_COPY(LoginController)

public:
    LoginController();

    void service(HttpRequest& request, HttpResponse& response);
    void   login(HttpRequest& request, HttpResponse& response); //登录
    void  logout(HttpRequest& request, HttpResponse& response); //退出
private:
    SqlHelper* m_pHelper;
    typedef void (LoginController::*pServFunc)(HttpRequest& request, HttpResponse& response);
    QMap<QString, pServFunc> m_mapFunc;

};

#endif // LOGINCONTROLLER_H
