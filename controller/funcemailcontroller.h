﻿#ifndef SYSEMAILCONTROLLER_H
#define SYSEMAILCONTROLLER_H

#include "global.h"
#include <QMap>
using namespace stefanfrings;

class FuncEmailController: public HttpRequestHandler
{
    Q_OBJECT
    Q_DISABLE_COPY(FuncEmailController)
public:
    FuncEmailController();
    void  service(HttpRequest& request, HttpResponse& response);
    void     send(HttpRequest& request, HttpResponse& response, SysUser& visitUser);  //页面获取服务器信息
private:
    SqlHelper* m_pHelper;
    typedef void (FuncEmailController::*pServFunc)(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    QMap<QString, pServFunc> m_mapFunc;
};

#endif // SYSEMAILCONTROLLER_H
