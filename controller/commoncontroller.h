﻿#ifndef COMMONCONTROLLER_H
#define COMMONCONTROLLER_H

#include "global.h"
#include <QMap>

using namespace stefanfrings;
class CommonController: public HttpRequestHandler
{
    Q_OBJECT
    Q_DISABLE_COPY(CommonController)
public:
    CommonController();
    void     service(HttpRequest& request, HttpResponse& response);
    void     sexType(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    void  selectDept(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    void  selectMenu(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    void  selectRole(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    void selectRoles(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    void getMenuTree(HttpRequest& request, HttpResponse& response, SysUser& visitUser);


private:
    SqlHelper* m_pHelper;
    typedef void (CommonController::*pServFunc)(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    QMap<QString, pServFunc> m_mapFunc;
};

#endif // COMMONCONTROLLER_H
