﻿#ifndef FUNCEXPORTCONTROLLER_H
#define FUNCEXPORTCONTROLLER_H

#include "global.h"
#include <QMap>
#include <QDateTime>
using namespace stefanfrings;

//测试数据
class TestUser
{
public:
    int id;
    int versions;
    QString createName;
    QDateTime createDate;
    QString name;
    int age;
    float balance;
    qint64 birthday;
    QString phone;
    QString sex;
};

class FuncExportController: public HttpRequestHandler
{
    Q_OBJECT
    Q_DISABLE_COPY(FuncExportController)
public:
    FuncExportController();
    void     service(HttpRequest& request, HttpResponse& response);
    void    datagrid(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    void  exportXlsx(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    void   exportCsv(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    void      upload(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
private:
    SqlHelper* m_pHelper;
    typedef void (FuncExportController::*pServFunc)(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    QMap<QString, pServFunc> m_mapFunc;

    QList<TestUser> userList;
};


#endif // FUNCEXPORTCONTROLLER_H
