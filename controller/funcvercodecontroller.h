﻿#ifndef VERCODECONTROLLER_H
#define VERCODECONTROLLER_H

#include "global.h"

using namespace stefanfrings;

class FuncVercodeController: public HttpRequestHandler {
    Q_OBJECT
    Q_DISABLE_COPY(FuncVercodeController)

public:
    FuncVercodeController();
    void service(HttpRequest& request, HttpResponse& response);
};

#endif // VERCODECONTROLLER_H
