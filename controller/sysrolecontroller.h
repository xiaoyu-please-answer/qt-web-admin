﻿#ifndef SYSROLECONTROLLER_H
#define SYSROLECONTROLLER_H

#include "global.h"
#include <QMap>

class SysRoleController: public HttpRequestHandler
{
    Q_OBJECT
    Q_DISABLE_COPY(SysRoleController)

public:
    SysRoleController();
    void      service(HttpRequest& request, HttpResponse& response);
    void      setMenu(HttpRequest& request, HttpResponse& response, SysUser& visitUser);   //获取权限树形数据
    void  getRoleName(HttpRequest& request, HttpResponse& response, SysUser& visitUser);   //获取角色名称
    void     datagrid(HttpRequest& request, HttpResponse& response, SysUser& visitUser);   //获取角色列表
    void         save(HttpRequest& request, HttpResponse& response, SysUser& visitUser);   //保存角色信息
    void      delRole(HttpRequest& request, HttpResponse& response, SysUser& visitUser);   //删除角色信息
    void saveRoleMenu(HttpRequest& request, HttpResponse& response, SysUser& visitUser);   //保存权限菜单

private:
    SqlHelper* m_pHelper;
    typedef void (SysRoleController::*pServFunc)(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    QMap<QString, pServFunc> m_mapFunc;
};

#endif // SYSROLECONTROLLER_H
