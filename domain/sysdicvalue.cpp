﻿#include "sysdicvalue.h"

SysDicValue::SysDicValue()
{
    this->tableName = QString("sys_dic_value");

    this->label = nullptr;
    this->value = nullptr;
    this->parentId = nullptr;
}

QStringList SysDicValue::getFieldList()
{
    QStringList fieldList = TableEntity::getEntityFieldList();
    fieldList << "label";
    fieldList << "value";
    fieldList << "parent_id";
    return fieldList;
}

void SysDicValue::setData(QMap<QString, QVariant>& valueMap)
{
    TableEntity::setEntityData(valueMap);
    this->label = valueMap["label"].toString();
    this->value = valueMap["value"].toString();
    this->parentId = valueMap["parent_id"].toString();
}

QString SysDicValue::getUpdateString()
{
    QStringList sqlSetList = TableEntity::getUpdateStringList();
    if(label != nullptr)
        sqlSetList << QString("label='%1'").arg(label);
    if(value != nullptr)
        sqlSetList << QString("value='%1'").arg(value);
    if(parentId != nullptr)
        sqlSetList << QString("parent_id=%1").arg(parentId);

    if(sqlSetList.size() == 0)
        return nullptr;

    QString sql = QString("update %1 set %2 where id=%3")
            .arg(getTableName()).arg(sqlSetList.join(",")).arg(id.toInt());
    return sql;
}

QString SysDicValue::getInsertString()
{
    QStringList fieldList;
    QStringList valueList;
    TableEntity::getEntityInsertString(fieldList, valueList);
    if(label != nullptr)
    {
        fieldList << "label";
        valueList << "'" + label + "'";
    }
    if(value != nullptr)
    {
        fieldList << "value";
        valueList << "'" + value + "'";
    }
    if(parentId != nullptr)
    {
        fieldList << "parent_id";
        valueList << parentId;
    }

    if(fieldList.size() == 0)
        return nullptr;

    QString sql = QString("insert into %1 (%2) values (%3)")
            .arg(getTableName())
            .arg(fieldList.join(","))
            .arg(valueList.join(","));
    return sql;
}
