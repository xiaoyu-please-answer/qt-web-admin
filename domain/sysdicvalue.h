﻿#ifndef SYSDICVALUE_H
#define SYSDICVALUE_H

#include "tableentity.h"

class SysDicValue:public TableEntity
{
public:
    SysDicValue();

    QStringList getFieldList();
    void setData(QMap<QString, QVariant>& valueMap);
    QString getUpdateString();
    QString getInsertString();

    QString getLabel(){return label;}
    QString getValue(){return value;}
    int     getParentId(){return parentId.toInt();}
    void setLabel(QString label){this->label = label;}
    void setValue(QString value){this->value = value;}
    void setParentId(int parentId){this->parentId = QString::number(parentId);}

private:
    QString label;      //显示值
    QString value;      //实际值
    QString parentId;   //父菜单ID
};

#endif // SYSDICVALUE_H
