﻿#include "syslog.h"

SysLog::SysLog()
{
    this->tableName = QString("sys_log");
    this->type = nullptr;
    this->name = nullptr;
    this->userName = nullptr;
    this->className = nullptr;
    this->methodName = nullptr;
    this->params = nullptr;
    this->ip = nullptr;
    this->broswer = nullptr;
    this->operatorTime.setTime_t(0);
    this->memo = nullptr;
}

SysLog::SysLog(QString createUser,   //备注
               int     type,         //日志类型
               QString name,         //名称
               QString userName,     //用户名
               QString className,    //操作类名
               QString methodName,   //方法名
               QString params,       //参数
               QString ip,           //IP
               QString broswer,      //浏览器类型
               QString memo )       //备注
{
    this->tableName = QString("sys_log");
    this->setVersion(0);
    this->setCreateName(createUser);
    this->setCreateDate(QDateTime::currentDateTime());
    this->setUpdateName(createUser);
    this->setUpdateDate(QDateTime::currentDateTime());
    this->type = QString::number(type);
    this->name = name;
    this->userName = userName;
    this->className = className;
    this->methodName = methodName;
    this->params = params;
    this->ip = ip;
    this->broswer = broswer;
    this->operatorTime = QDateTime::currentDateTime();
    this->memo = memo;
}

QStringList SysLog::getFieldList()
{
    QStringList fieldList = TableEntity::getEntityFieldList();
    fieldList << "type";
    fieldList << "name";
    fieldList << "user_name";
    fieldList << "class_name";
    fieldList << "method_name";
    fieldList << "params";
    fieldList << "ip";
    fieldList << "broswer";
    fieldList << "operator_time";
    fieldList << "memo";
    return fieldList;
}

void SysLog::setData(QMap<QString, QVariant>& valueMap)
{
    TableEntity::setEntityData(valueMap);
    this->type = valueMap["type"].toString();
    this->name = valueMap["name"].toString();
    this->userName = valueMap["user_name"].toString();
    this->className = valueMap["class_name"].toString();
    this->methodName = valueMap["method_name"].toString();
    this->params = valueMap["params"].toString();
    this->ip = valueMap["ip"].toString();
    this->broswer = valueMap["broswer"].toString();
    this->operatorTime = valueMap["operator_time"].toDateTime();
    this->memo = valueMap["memo"].toString();
}

QString SysLog::getUpdateString()
{
    QStringList updateList = getUpdateStringList();
    if(type != nullptr)
        updateList << QString("type='%1'").arg(type);
    if(name != nullptr)
        updateList << QString("name='%1'").arg(name);
    if(userName != nullptr)
        updateList << QString("user_name='%1'").arg(userName);
    if(className != nullptr)
        updateList << QString("class_name='%1'").arg(className);
    if(methodName != nullptr)
        updateList << QString("method_name='%1'").arg(methodName);
    if(params != nullptr)
        updateList << QString("params='%1'").arg(params);
    if(ip != nullptr)
        updateList << QString("ip='%1'").arg(ip);
    if(broswer != nullptr)
        updateList << QString("broswer='%1'").arg(broswer);
    if(operatorTime.toTime_t() > 0)
        updateList << QString("operator_time='%1'").arg(operatorTime.toString("yyyy-MM-dd hh:mm:ss"));
    if(memo != nullptr)
        updateList << QString("memo='%1'").arg(memo);

    QString sql = QString("update %1 set %2 where id=%3")
            .arg(getTableName()).arg(updateList.join(",")).arg(id.toInt());
    return sql;
}

QString SysLog::getInsertString()
{
    QStringList fieldList;
    QStringList valueList;
    TableEntity::getEntityInsertString(fieldList, valueList);

    if(type != nullptr)
    {
        fieldList << "type";
        valueList << "'" + type + "'";
    }
    if(name != nullptr)
    {
        fieldList << "name";
        valueList << "'" + name + "'";
    }

    if(userName != nullptr)
    {
        fieldList << "user_name";
        valueList << "'" + userName + "'";
    }
    if(className != nullptr)
    {
        fieldList << "class_name";
        valueList << "'" + className + "'";
    }
    if(methodName != nullptr)
    {
        fieldList << "method_name";
        valueList << "'" + methodName + "'";
    }
    if(params != nullptr)
    {
        fieldList << "params";
        valueList << "'" + params + "'";
    }

    if(ip != nullptr)
    {
        fieldList << "ip";
        valueList << "'" + ip + "'";
    }

    if(broswer != nullptr)
    {
        fieldList << "broswer";
        valueList << "'" + broswer + "'";
    }

    if(operatorTime.toTime_t() > 0)
    {
        fieldList << "operator_time";
        valueList << "'" + operatorTime.toString("yyyy-MM-dd hh:mm:ss") + "'";
    }

    if(memo != nullptr)
    {
        fieldList << "memo";
        valueList << "'" + memo + "'";
    }

    if(fieldList.size() == 0)
        return nullptr;

    QString sql1 = fieldList.join(",");
    QString sql2 = valueList.join(",");
    QString sql = QString("insert into %1 (%2) values (%3)")
            .arg(getTableName())
            .arg(sql1)
            .arg(sql2);
    return sql;
}

QJsonObject SysLog::toJson()
{
    QJsonObject obj;
    obj.insert("id",         getId());
    obj.insert("version",    getVersion());
    obj.insert("createName", getCreateName());
    obj.insert("createDate", getCreateDate().toMSecsSinceEpoch());
    obj.insert("updateName", getUpdateName());
    obj.insert("updateDate", getUpdateDate().toMSecsSinceEpoch());

    obj.insert("type",       getType());
    obj.insert("name",       getName());
    obj.insert("userName",   getUserName());
    obj.insert("className",  getClassName());
    obj.insert("methodName", getMethodName());
    obj.insert("params",     getParams());
    obj.insert("ip",         getIP());
    obj.insert("broswer",    getBroswer());
    obj.insert("operatorTime",getOperatorTime().toMSecsSinceEpoch());
    obj.insert("memo",       getMemo());
    return obj;
}
