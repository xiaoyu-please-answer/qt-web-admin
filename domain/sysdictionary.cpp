﻿#include "sysdictionary.h"

SysDictionary::SysDictionary()
{
    this->tableName = QString("sys_dic");

    this->code = nullptr;
    this->name = nullptr;
    this->memo = nullptr;

}

QStringList SysDictionary::getFieldList()
{
    QStringList fieldList = TableEntity::getEntityFieldList();
    fieldList << "code";
    fieldList << "name";
    fieldList << "memo";
    return fieldList;
}

void SysDictionary::setData(QMap<QString, QVariant>& valueMap)
{
    TableEntity::setEntityData(valueMap);
    this->code = valueMap["code"].toString();
    this->name = valueMap["name"].toString();
    this->memo = valueMap["memo"].toString();
}
QString SysDictionary::getUpdateString()
{

    QStringList sqlSetList = getUpdateStringList();
    if(code != nullptr)
        sqlSetList << QString("code='%1'").arg(code);
    if(name != nullptr)
        sqlSetList << QString("name='%1'").arg(name);
    if(memo != nullptr)
        sqlSetList << QString("memo='%1'").arg(memo);

    if(sqlSetList.size() == 0)
        return nullptr;

    QString sql = QString("update %1 set %2 where id=%3")
            .arg(getTableName()).arg(sqlSetList.join(",")).arg(id.toInt());
    return sql;
}
QString SysDictionary::getInsertString()
{

    QStringList fieldList;
    QStringList valueList;
    TableEntity::getEntityInsertString(fieldList, valueList);

    if(code != nullptr)
    {
        fieldList << "code";
        valueList << "'" + code + "'";
    }
    if(name != nullptr)
    {
        fieldList << "name";
        valueList << "'" + name + "'";
    }
    if(memo != nullptr)
    {
        fieldList << "memo";
        valueList << "'" + memo + "'";
    }

    if(fieldList.size() == 0)
        return nullptr;

    QString sql = QString("insert into %1 (%2) values (%3)")
            .arg(getTableName())
            .arg(fieldList.join(","))
            .arg(valueList.join(","));
    return sql;
}
