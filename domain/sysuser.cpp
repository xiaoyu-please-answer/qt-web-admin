﻿#include "sysuser.h"

SysUser::SysUser():TableEntity()
{
    this->tableName = QString("sys_user");

    this->userName = nullptr;
    this->realName = nullptr;
    this->password = nullptr;
    this->salt = nullptr;
    this->headImgUrl = nullptr;
    this->status = nullptr;
    this->sex = nullptr;
    this->deptId = nullptr;
    this->email = nullptr;
    this->telNo = nullptr;
    this->userNo = nullptr;
    this->memo = nullptr;

}

void SysUser::setData(QMap<QString, QVariant>& valueMap)
{
    TableEntity::setEntityData(valueMap);
    this->userName = valueMap["user_name"].toString();
    this->realName = valueMap["real_name"].toString();
    this->password = valueMap["password"].toString();
    this->salt = valueMap["salt"].toString();
    this->headImgUrl = valueMap["head_img_url"].toString();
    this->status = valueMap["status"].toString();
    this->sex = valueMap["sex"].toString();
    this->deptId = valueMap["dept_id"].toString();
    this->email = valueMap["email"].toString();
    this->telNo = valueMap["tel_no"].toString();
    this->userNo = valueMap["user_no"].toString();
    this->memo = valueMap["memo"].toString();
}

QStringList SysUser::getFieldList()
{
    QStringList fieldList = TableEntity::getEntityFieldList();
    fieldList << "user_name";
    fieldList << "real_name";
    fieldList << "password";
    fieldList << "salt";
    fieldList << "head_img_url";
    fieldList << "status";
    fieldList << "sex";
    fieldList << "dept_id";
    fieldList << "email";
    fieldList << "tel_no";
    fieldList << "user_no";
    fieldList << "memo";
    return fieldList;
}

QString SysUser::getUpdateString()
{
    QStringList sqlSetList = getUpdateStringList();
    if(userName != nullptr)
        sqlSetList << QString("user_name='%1'").arg(userName);
    if(realName != nullptr)
        sqlSetList << QString("real_name='%1'").arg(realName);
    if(password != nullptr)
        sqlSetList << QString("password='%1'").arg(password);
    if(salt != nullptr)
        sqlSetList << QString("salt='%1'").arg(salt);
    if(headImgUrl != nullptr)
        sqlSetList << QString("head_img_url='%1'").arg(headImgUrl);
    if(status != nullptr)
        sqlSetList << QString("status='%1'").arg(status);
    if(sex != nullptr)
        sqlSetList << QString("sex='%1'").arg(sex);
    if(deptId != nullptr)
        sqlSetList << QString("dept_id=%1").arg(deptId);
    if(email != nullptr)
        sqlSetList << QString("email='%1'").arg(email);
    if(telNo != nullptr)
        sqlSetList << QString("tel_no='%1'").arg(telNo);
    if(userNo != nullptr)
        sqlSetList << QString("user_no='%1'").arg(userNo);
    if(memo != nullptr)
        sqlSetList << QString("memo='%1'").arg(memo);

    if(sqlSetList.size() == 0)
        return nullptr;

    QString sql = QString("update %1 set %2 where id=%3")
            .arg(getTableName()).arg(sqlSetList.join(",")).arg(id.toInt());
    return sql;
}


QString SysUser::getInsertString()
{
    QStringList fieldList;
    QStringList valueList;
    TableEntity::getEntityInsertString(fieldList, valueList);

    if(userName != nullptr)
    {
        fieldList << "user_name";
        valueList << "'" + userName + "'";
    }
    if(realName != nullptr)
    {
        fieldList << "real_name";
        valueList << "'" + realName + "'";
    }
    if(password != nullptr)
    {
        fieldList << "password";
        valueList << "'" + password + "'";
    }
    if(salt != nullptr)
    {
        fieldList << "salt";
        valueList << "'" + salt + "'";
    }
    if(headImgUrl != nullptr)
    {
        fieldList << "head_img_url";
        valueList << "'" + headImgUrl + "'";
    }
    if(status != nullptr)
    {
        fieldList << "status";
        valueList << "'" + status + "'";
    }
    if(sex != nullptr)
    {
        fieldList << "sex";
        valueList << "'" + sex + "'";
    }
    if(deptId != nullptr)
    {
        fieldList << "dept_id";
        valueList << deptId;
    }
    if(email != nullptr)
    {
        fieldList << "email";
        valueList << "'" + email + "'";
    }
    if(telNo != nullptr)
    {
        fieldList << "tel_no";
        valueList << "'" + telNo + "'";
    }
    if(userNo != nullptr)
    {
        fieldList << "user_no";
        valueList << "'" + userNo + "'";
    }
    if(memo != nullptr)
    {
        fieldList << "memo";
        valueList << "'" + memo + "'";
    }

    if(fieldList.size() == 0)
        return nullptr;

    QString sql = QString("insert into %1 (%2) values (%3)")
            .arg(getTableName())
            .arg(fieldList.join(","))
            .arg(valueList.join(","));
    return sql;
}

