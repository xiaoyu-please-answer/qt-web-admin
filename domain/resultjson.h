﻿#ifndef RESULTJSON_H
#define RESULTJSON_H

#include <QJsonObject>

//JSON返回数据
class ResultJson
{
public:
    ResultJson();
    ResultJson(int code, bool success, QString msg);
    QString toString();

    void setCode(int code);
    void setSuccess(bool success);
    void setMsg(QString msg);
    void setProp(QString prop, QJsonValue value);
    void setData(QString key, QJsonValue value);
    void setData(QJsonValue value);
    void failedMsg(QString msg);
    void okMsg(QString msg);


private:
    QJsonObject ret;
    QJsonObject data;
};

#endif // RESULTJSON_H
