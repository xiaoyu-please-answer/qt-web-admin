﻿#include "sysmessage.h"

#include "tableentity.h"

SysMessage::SysMessage()
{
    this->tableName = QString("func_sys_message");
    this->title = nullptr;
    this->content = nullptr;
    this->type = nullptr;
    this->status = nullptr;
    this->publicTime.setTime_t(0);
}


QStringList SysMessage::getFieldList()
{
    QStringList fieldList = TableEntity::getEntityFieldList();
    fieldList << "title";
    fieldList << "content";
    fieldList << "type";
    fieldList << "status";
    fieldList << "public_time";
    return fieldList;
}


void SysMessage::setData(QMap<QString, QVariant>& valueMap)
{
    TableEntity::setEntityData(valueMap);
    this->title = valueMap["title"].toString();
    this->content = valueMap["content"].toString();
    this->type = valueMap["type"].toString();
    this->status = valueMap["status"].toString();
    this->publicTime = valueMap["public_time"].toDateTime();
}

QString SysMessage::getUpdateString()
{
    QStringList updateList = getUpdateStringList();
    if(title != nullptr)
        updateList << QString("title='%1'").arg(title);
    if(content != nullptr)
        updateList << QString("content='%1'").arg(content);
    if(type != nullptr)
        updateList << QString("type='%1'").arg(type);
    if(status != nullptr)
        updateList << QString("status='%1'").arg(status);
    if(publicTime.toTime_t() > 0)
        updateList << QString("public_time='%1'").arg(publicTime.toString("yyyy-MM-dd hh:mm:ss"));

    QString sql = QString("update %1 set %2 where id=%3")
            .arg(getTableName()).arg(updateList.join(",")).arg(id.toInt());
    return sql;
}


QString SysMessage::getInsertString()
{
    QStringList fieldList;
    QStringList valueList;
    TableEntity::getEntityInsertString(fieldList, valueList);

    if(title != nullptr)
    {
        fieldList << "title";
        valueList << "'" + title + "'";
    }
    if(content != nullptr)
    {
        fieldList << "content";
        valueList << "'" + content + "'";
    }
    if(type != nullptr)
    {
        fieldList << "type";
        valueList << "'" + type + "'";
    }
    if(status != nullptr)
    {
        fieldList << "status";
        valueList << "'" + status + "'";
    }

    if(publicTime.toTime_t() > 0)
    {
        fieldList << "public_time";
        valueList << "'" + publicTime.toString("yyyy-MM-dd hh:mm:ss") + "'";
    }
    if(fieldList.size() == 0)
        return nullptr;

    QString sql1 = fieldList.join(",");
    QString sql2 = valueList.join(",");
    QString sql = QString("insert into %1 (%2) values (%3)")
            .arg(getTableName())
            .arg(sql1)
            .arg(sql2);
    return sql;
}

QJsonObject SysMessage::toJson()
{
    QJsonObject obj;
    obj.insert("id",         getId());
    obj.insert("version",    getVersion());
    obj.insert("createName", getCreateName());
    obj.insert("createDate", getCreateDate().toMSecsSinceEpoch());
    obj.insert("updateName", getUpdateName());
    obj.insert("updateDate", getUpdateDate().toMSecsSinceEpoch());

    obj.insert("title",      getTitle());
    obj.insert("content",    getContent());
    obj.insert("type",       getType());
    obj.insert("status",     getStatus());
    obj.insert("publicTime", getPublicTime().toMSecsSinceEpoch());
    return obj;
}
