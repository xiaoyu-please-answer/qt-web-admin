﻿#include "sysrole.h"

SysRole::SysRole()
{
    this->tableName = QString("sys_role");
    this->name = nullptr;
    this->parentId = nullptr;
    this->roleNo = nullptr;
    this->orderNo = nullptr;
    this->memo = nullptr;
}

QStringList SysRole::getFieldList()
{
    QStringList fieldList = TableEntity::getEntityFieldList();
    fieldList << "name";
    fieldList << "parent_id";
    fieldList << "role_no";
    fieldList << "order_no";
    fieldList << "memo";
    return fieldList;
}
void SysRole::setData(QMap<QString, QVariant>& valueMap)
{
    TableEntity::setEntityData(valueMap);
    this->name = valueMap["name"].toString();
    this->parentId = valueMap["parent_id"].toString();
    this->roleNo = valueMap["role_no"].toString();
    this->orderNo = valueMap["order_no"].toString();
    this->memo = valueMap["memo"].toString();
}
QString SysRole::getUpdateString()
{
    QStringList sqlSetList = getUpdateStringList();
    if(name != nullptr)
        sqlSetList << QString("name='%1'").arg(name);
    if(parentId != nullptr)
        sqlSetList << QString("parent_id=%1").arg(parentId);
    if(roleNo != nullptr)
        sqlSetList << QString("role_no='%1'").arg(roleNo);
    if(orderNo != nullptr)
        sqlSetList << QString("order_no=%1").arg(orderNo);
    if(memo != nullptr)
        sqlSetList << QString("memo='%1'").arg(memo);

    QString sql = QString("update %1 set %2 where id=%3")
            .arg(getTableName()).arg(sqlSetList.join(",")).arg(id.toInt());
    return sql;

}
QString SysRole::getInsertString()
{

    QStringList fieldList;
    QStringList valueList;
    TableEntity::getEntityInsertString(fieldList, valueList);
    if(name != nullptr)
    {
        fieldList << "name";
        valueList << "'" + name + "'";
    }
    if(parentId != nullptr)
    {
        fieldList << "parent_id";
        valueList << parentId;
    }
    if(roleNo != nullptr)
    {
        fieldList << "role_no";
        valueList << "'" + roleNo + "'";
    }
    if(orderNo != nullptr)
    {
        fieldList << "order_no";
        valueList << orderNo;
    }
    if(memo != nullptr)
    {
        fieldList << "memo";
        valueList << "'" + memo + "'";
    }

    if(fieldList.size() == 0)
        return nullptr;

    QString sql = QString("insert into %1 (%2) values (%3)")
            .arg(getTableName())
            .arg(fieldList.join(","))
            .arg(valueList.join(","));
    return sql;
}
QJsonObject SysRole::toJson()
{
    QJsonObject obj;
    obj.insert("id",         getId());
    obj.insert("version",    getVersion());
    obj.insert("createName", getCreateName());
    obj.insert("createDate", getCreateDate().toMSecsSinceEpoch());
    obj.insert("updateName", getUpdateName());
    obj.insert("updateDate", getUpdateDate().toMSecsSinceEpoch());

    obj.insert("name",       getName());
    obj.insert("parentId",   getParentId());
    obj.insert("roleNo",     getRoleNo());
    obj.insert("orderNo",    getOrderNo());
    obj.insert("memo",       getMemo());

    return obj;
}
