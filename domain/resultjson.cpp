﻿#include "resultjson.h"
#include "global.h"

ResultJson::ResultJson()
{
    ret.insert("code", 0);
    ret.insert("success", true);
    ret.insert("msg", "ok");
    ret.insert("data", data);
}

ResultJson::ResultJson(int code, bool success, QString msg)
{
    ret.insert("code", code);
    ret.insert("msg", msg);
    ret.insert("success", success);
    ret.insert("data", data);
}

void ResultJson::setCode(int code)
{
    ret.insert("code", code);
}

void ResultJson::setProp(QString prop, QJsonValue value)
{
    ret.insert(prop, value);
}

void ResultJson::setSuccess(bool success)
{
    ret.insert("success", success);
}

void ResultJson::setMsg(QString msg)
{
    ret.insert("msg", msg);
}

void ResultJson::setData(QString key, QJsonValue value)
{
    data.insert(key, value);
    ret.insert("data", data);
}

void ResultJson::setData(QJsonValue value)
{
    auto it = data.begin();
    while (it != data.end()) {
        data.erase(it);
        it++;
    }

    ret.insert("data", value);
}

QString ResultJson::toString()
{
    return GlobalFunc::JsonToString(ret);
}

void ResultJson::okMsg(QString msg)
{
    ret.insert("msg", msg);
    ret.insert("success", true);
}

void ResultJson::failedMsg(QString msg)
{
    ret.insert("msg", msg);
    ret.insert("success", false);
}
