﻿#include "sysmenu.h"

SysMenu::SysMenu()
{
    this->tableName = QString("sys_menu");
    this->name = nullptr;
    this->parentId = nullptr;
    this->type = nullptr;
    this->url = nullptr;
    this->icon = nullptr;
    this->orderNo = nullptr;
    this->memo = nullptr;
}

void SysMenu::setData(QMap<QString, QVariant>& valueMap)
{
    TableEntity::setEntityData(valueMap);
    this->name = valueMap["name"].toString();
    this->parentId = valueMap["parent_id"].toString();
    this->type = valueMap["type"].toString();
    this->url = valueMap["url"].toString();
    this->icon = valueMap["icon"].toString();
    this->orderNo = valueMap["order_no"].toString();
    this->memo = valueMap["memo"].toString();
}

QStringList SysMenu::getFieldList()
{
    QStringList fieldList = TableEntity::getEntityFieldList();
    fieldList << "name";
    fieldList << "parent_id";
    fieldList << "type";
    fieldList << "url";
    fieldList << "icon";
    fieldList << "order_no";
    fieldList << "memo";
    return fieldList;
}

QString SysMenu::getUpdateString()
{
    QStringList sqlSetList = getUpdateStringList();
    if(name != nullptr)
        sqlSetList << QString("name='%1'").arg(name);
    if(parentId != nullptr)
        sqlSetList << QString("parent_id=%1").arg(parentId);
    if(type != nullptr)
        sqlSetList << QString("type='%1'").arg(type);
    if(url != nullptr)
        sqlSetList << QString("url='%1'").arg(url);
    if(icon != nullptr)
        sqlSetList << QString("icon='%1'").arg(icon);
    if(orderNo != nullptr)
        sqlSetList << QString("order_no=%1").arg(orderNo);
    if(memo != nullptr)
        sqlSetList << QString("memo='%1'").arg(memo);

    QString sql = QString("update %1 set %2 where id=%3")
            .arg(getTableName()).arg(sqlSetList.join(",")).arg(id.toInt());
    return sql;
}

QString SysMenu::getInsertString()
{
    QStringList fieldList;
    QStringList valueList;
    TableEntity::getEntityInsertString(fieldList, valueList);

    if(name != nullptr)
    {
        fieldList << "name";
        valueList << "'" + name + "'";
    }
    if(parentId != nullptr)
    {
        fieldList << "parent_id";
        valueList << parentId;
    }
    if(type != nullptr)
    {
        fieldList << "type";
        valueList << "'" + type + "'";
    }
    if(url != nullptr)
    {
        fieldList << "url";
        valueList << "'" + url + "'";
    }
    if(icon != nullptr)
    {
        fieldList << "icon";
        valueList << "'" + icon + "'";
    }
    if(orderNo != nullptr)
    {
        fieldList << "order_no";
        valueList << orderNo;
    }
    if(memo != nullptr)
    {
        fieldList << "memo";
        valueList << "'" + memo + "'";
    }

    if(fieldList.size() == 0)
        return nullptr;

    QString sql = QString("insert into %1 (%2) values (%3)")
            .arg(getTableName())
            .arg(fieldList.join(","))
            .arg(valueList.join(","));
    return sql;
}

QJsonObject SysMenu::toJson()
{
    QJsonObject obj;
    obj.insert("id",   getId());
    obj.insert("version",    getVersion());
    obj.insert("createName", getCreateName());
    obj.insert("createDate", getCreateDate().toMSecsSinceEpoch());
    obj.insert("updateName", getUpdateName());
    obj.insert("updateDate", getUpdateDate().toMSecsSinceEpoch());

    obj.insert("name",       getName());
    obj.insert("parentId",   getParentId());
    obj.insert("icon",       getIcon());
    obj.insert("url",        getUrl());
    obj.insert("type",       getType());
    obj.insert("orderNo",    getOrderNo());
    obj.insert("memo",       getMemo());

    return obj;
}
