﻿#ifndef SYSLOG_H
#define SYSLOG_H

#include "tableentity.h"

class SysLog : public TableEntity
{
public:
    static const int LOGIN = 1;     //登录
    static const int LOGOUT = 2;    //登出
    static const int SAVE = 3;      //保存
    static const int INSERT = 4;    //新增
    static const int DEL = 5;       //删除
    static const int UPDATE = 6;    //更新
    static const int TIMER = 7;     //定时任务
    static const int NORMAL = 8;    //正常
    static const int ABNORMAL = 9;  //异常
    static const int OTHER = 10;    //其他

    SysLog();
    SysLog(QString createUser,   //备注
           int     type,         //日志类型
           QString name,         //名称
           QString userName,     //用户名
           QString className,    //操作类名
           QString methodName,   //方法名
           QString params,       //参数
           QString ip,           //IP
           QString broswer,      //浏览器类型
           QString memo );       //备注

    QStringList getFieldList();
    void        setData(QMap<QString, QVariant>& valueMap);
    QString     getUpdateString();
    QString     getInsertString();
    QJsonObject toJson();

    //setter getter
    QString   getType()         {return type;}
    QString   getName()         {return name;}
    QString   getUserName()     {return userName;}
    QString   getClassName()    {return className;}
    QString   getMethodName()   {return methodName;}
    QString   getParams()       {return params;}
    QString   getIP()           {return ip;}
    QString   getBroswer()      {return broswer;}
    QDateTime getOperatorTime()  {return operatorTime;}
    QString   getMemo()         {return memo;}

    void setType(QString type)                  {this->type = type;}
    void setName(QString name)                  {this->name = name;}
    void setUserName(QString userName)          {this->userName = userName;}
    void setClassName(QString className)        {this->className = className;}
    void setMethodName(QString methodName)      {this->methodName = methodName;}
    void setParams(QString params)              {this->params = params;}
    void setIP(QString ip)                      {this->ip = ip;}
    void setBroswer(QString broswer)            {this->broswer = broswer;}
    void setOperatorTime(QDateTime operatorTime){this->operatorTime = operatorTime;}
    void setMemo(QString memo)                  {this->memo = memo;}


private:

    QString   type;          //日志类型
    QString   name;          //名称
    QString   userName;      //用户名
    QString   className;     //操作类名
    QString   methodName;    //方法名
    QString   params;        //参数
    QString   ip;            //IP
    QString   broswer;       //浏览器类型
    QDateTime operatorTime;   //操作时间
    QString   memo;          //备注
};

#endif // SYSLOG_H

