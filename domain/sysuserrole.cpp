﻿#include "sysuserrole.h"

SysUserRole::SysUserRole()
{
    this->tableName = QString("sys_user_role");
    this->roleId = nullptr;
    this->userId = nullptr;
}

QStringList SysUserRole::getFieldList()
{
    QStringList fieldList = TableEntity::getEntityFieldList();
    fieldList << "user_id";
    fieldList << "role_id";
    return fieldList;
}
void SysUserRole::setData(QMap<QString, QVariant>& valueMap)
{
    TableEntity::setEntityData(valueMap);
    this->roleId = valueMap["role_id"].toString();
    this->userId = valueMap["user_id"].toString();
}
QString SysUserRole::getUpdateString()
{
    QStringList sqlSetList = getUpdateStringList();
    if(userId != nullptr)
        sqlSetList << QString("user_id=%1").arg(userId);
    if(roleId != nullptr)
        sqlSetList << QString("role_id=%1").arg(roleId);

    QString sql = QString("update %1 set %2 where id=%3")
            .arg(getTableName()).arg(sqlSetList.join(",")).arg(id.toInt());
    return sql;

}
QString SysUserRole::getInsertString()
{

    QStringList fieldList;
    QStringList valueList;
    TableEntity::getEntityInsertString(fieldList, valueList);

    if(userId != nullptr)
    {
        fieldList << "user_id";
        valueList << userId;
    }
    if(roleId != nullptr)
    {
        fieldList << "role_id";
        valueList << roleId;
    }

    if(fieldList.size() == 0)
        return nullptr;

    QString sql = QString("insert into %1 (%2) values (%3)")
            .arg(getTableName())
            .arg(fieldList.join(","))
            .arg(valueList.join(","));
    return sql;
}
QJsonObject SysUserRole::toJson()
{
    QJsonObject obj;
    obj.insert("id",         getId());
    obj.insert("version",    getVersion());
    obj.insert("createName", getCreateName());
    obj.insert("createDate", getCreateDate().toMSecsSinceEpoch());
    obj.insert("updateName", getUpdateName());
    obj.insert("updateDate", getUpdateDate().toMSecsSinceEpoch());

    obj.insert("userId",     getUserId());
    obj.insert("roleId",     getRoleId());

    return obj;
}
