#-------------------------------------------------
#
# Project created by QtCreator 2023-08-04T19:13:16
#
#-------------------------------------------------

QT       += core webenginewidgets network sql
CONFIG += c++11 qt
TEMPLATE = app
CODECFORTR = utf-8
DEFAULTCODEC  = utf-8

DEFINES += ISGUI
contains(DEFINES, ISGUI)
{
    QT += gui
    CONFIG -= console
}
!contains(DEFINES, ISGUI)
{
    QT -= gui
    CONFIG += console
}


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtWebAdmin
TEMPLATE = app


SOURCES += main.cpp\
    global.cpp \
    requestmapper.cpp \
    db/sqlhelper.cpp \
    controller/logincontroller.cpp \
    controller/sysusercontroller.cpp \
    controller/sysmenucontroller.cpp \
    controller/sysmsgcontroller.cpp \
    controller/commoncontroller.cpp \
    controller/sysdiccontroller.cpp \
    controller/sysdeptcontroller.cpp \
    controller/sysrolecontroller.cpp \
    controller/syslogcontroller.cpp \
    controller/sysusermsgcontroller.cpp \
    controller/funcmonitorcontroller.cpp \
    controller/funcvercodecontroller.cpp \
    controller/funcemailcontroller.cpp \
    controller/funcsmscontroller.cpp \
    controller/funcloadcontroller.cpp \
    controller/funcexportcontroller.cpp \
    domain/resultjson.cpp \
    domain/sysuser.cpp \
    domain/tableentity.cpp \
    domain/sysdept.cpp \
    domain/sysdictionary.cpp \
    domain/sysdicvalue.cpp \
    domain/sysmenu.cpp \
    domain/sysrole.cpp \
    domain/sysrolemenu.cpp \
    domain/sysuserrole.cpp \
    domain/syslog.cpp \
    domain/sysmessage.cpp \
    domain/sysusermessage.cpp \
    utils/jwtutil.cpp \
    utils/cacheapi.cpp \
    utils/responseutil.cpp \
    utils/dictionaryutil.cpp \
    utils/monitorutil.cpp \
    utils/excelutil.cpp

HEADERS  += \
    global.h \
    requestmapper.h \
    db/sqlhelper.h \
    controller/logincontroller.h \
    controller/sysusercontroller.h \
    controller/sysmenucontroller.h \
    controller/sysmsgcontroller.h \
    controller/funcemailcontroller.h \
    controller/funcmonitorcontroller.h \
    controller/funcsmscontroller.h \
    controller/funcvercodecontroller.h \
    controller/funcloadcontroller.h \
    controller/funcexportcontroller.h \
    controller/commoncontroller.h \
    controller/sysdiccontroller.h \
    controller/sysdeptcontroller.h \
    controller/sysrolecontroller.h \
    controller/syslogcontroller.h \
    controller/sysusermsgcontroller.h \
    domain/resultjson.h \
    domain/sysuser.h \
    domain/tableentity.h \
    domain/sysdept.h \
    domain/sysdictionary.h \
    domain/sysdicvalue.h \
    domain/sysmenu.h \
    domain/sysrole.h \
    domain/sysrolemenu.h \
    domain/sysuserrole.h \
    domain/syslog.h \
    domain/sysmessage.h \
    domain/sysusermessage.h \
    utils/jwtutil.h \
    utils/cacheapi.h \
    utils/responseutil.h\
    utils/dictionaryutil.h \
    utils/sysinfo.hpp \
    utils/monitorutil.h \
    utils/constexprqstring.hpp \
    utils/excelutil.h

FORMS    +=

OTHER_FILES += etc/* etc/docroot/* etc/templates/* etc/ssl/* logs/* etc/db/* readme.md


#---------------------------------------------------------------------------------------
# The following lines include the sources of the QtWebAppLib library
#---------------------------------------------------------------------------------------

include(./QtWebApp/logging/logging.pri)
include(./QtWebApp/httpserver/httpserver.pri)
include(./QtWebApp/templateengine/templateengine.pri)
include(./QJsonWebToken/src/qjsonwebtoken.pri)
include($$PWD/JQLibrary/JQCPUMonitor.pri )
include($$PWD/QtXlsxWriter/src/xlsx/qtxlsx.pri)

