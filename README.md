# QtWebAdmin

#### 介绍

1. QtWebAdmin是基于QtWebApp+QJsonWebToken+SQLite的前后端分离轻量级C++快速开发框架，以QtWebApp作为底层cppweb，前端采用layui。
2. QtWebAdmin内置部门管理、用户管理、角色管理、菜单管理、数据数据字典，常用系统监控等基础功能，整合了layui前端常用组件。
3. QtWebAdmin定位于以Qt/C++为后端，整合前端框架，引入SQLite作为数据库，自定义了CacheApi作为内存缓存区。
4. QtWebAdmin是我在长期使用Qt/C++和javaWeb开发过程中萌生出的想法，希望能结合C++和web，在特殊场景下使用。
5. 本人理解的特殊场景就是底层是C++基座，不想用JNI做桥梁，又需要WEB页面，本人遇到的工业现场大部分都是这个情况。

#### 截图展示

截图包括桌面端和浏览器端页面的展示

1. ![screen1](./screen/screen1.jpg)
2. ![screen2](./screen/screen2.jpg)
3. ![screen3](./screen/screen3.png)
4. ![screen4](./screen/screen4.jpg)
5. ![screen5](./screen/screen5.gif)
6. ![screen6](./screen/screen6.gif)

#### 软件架构

软件架构说明
本系统为了方便，采用了Sqlite3。
感谢开源项目：
QtWebApp
Layui
QJsonWebToken
QSystemInfo 
JQCPUMonitor
QtXlsx

#### 安装教程

1. 本软件使用Qt5.6.1编译，应支持Qt5以上版本，项目中每一次连接访问都是开了个新线程，因此在高版本中，会提示线程安全无法访问数据，导致无法登录，建议使用低版本，目前Qt5.6和Qt5.7是可以的
2. 服务器监控部分只做了windows版本
3. 初次运行时，应执行etc/db下的create.sql，在数据库中创建好表
4. 程序有三种发布方式：
   1）桌面端形式发布，启动时启动桌面端页面
   2）前后端一体发布，通过浏览器打开页面查看
   3）前后端独立发布，通过浏览器打开查看页面

#### 使用说明

1. WEB发布方式：chrome浏览器打开http://localhost:9005/
2. 桌面发布方式：编译时，增加ISGUI编译开关，直接运行即可
